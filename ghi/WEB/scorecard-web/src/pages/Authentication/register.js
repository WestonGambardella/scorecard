import React, { useState, useEffect } from 'react';
import { useAuthContext } from '../../Context/AuthContext';
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { setToken } from '../../Slices/authSlice'

function Register() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [homeCourse, setHomeCourse] = useState('');
    const { register, login } = useAuthContext();
    const navigate = useNavigate();
    const dispatch = useDispatch()
    const token = useSelector((state) => state.auth.token)


    useEffect(()=> {
        if (token !== null){
            navigate('/')
        }
    }, [token, navigate])


    const handleRegister = async (event) => {
        event.preventDefault();
        const registerData = {}
        registerData.username = username
        registerData.password = password
        registerData.email = email
        registerData.first_name = firstName
        registerData.last_name = lastName
        registerData.home_course = homeCourse
        const registerResponse = await register(registerData)
        if (registerResponse === true) {
            const loginResponse = await login(registerData.username, registerData.password)
            if (loginResponse !== null) {
                dispatch(setToken(loginResponse))
            }
        }
    };

    const handleUsernameChange = (event) => {
        setUsername(event.target.value);
    };

    const handleHomeCourseChange = (event) => {
        setHomeCourse(event.target.value);
    };

    const handleFirstNameChange = (event) => {
        setFirstName(event.target.value);
    };  
    const handleLastNameChange = (event) => {
        setLastName(event.target.value);
    };    
    
    const handleEmailChange = (event) => {
        setEmail(event.target.value);
    };

    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    };

    const handleLoginNav = () => {
        navigate('/login')
    }

    return (
        <div>
            <form onSubmit={handleRegister}>
                <input
                    type="text"
                    placeholder="Username"
                    value={username}
                    onChange={handleUsernameChange}
                />
                <input
                    type="password"
                    placeholder="Password"
                    value={password}
                    onChange={handlePasswordChange}
                />
                <input
                    type="email"
                    placeholder="Email"
                    value={email}
                    onChange={handleEmailChange}
                />
                <input
                    type="text"
                    placeholder="First Name"
                    value={firstName}
                    onChange={handleFirstNameChange}
                />
                <input
                    type="text"
                    placeholder="Last Name"
                    value={lastName}
                    onChange={handleLastNameChange}
                />
                <input
                    type="text"
                    placeholder="Home Course"
                    value={homeCourse}
                    onChange={handleHomeCourseChange}
                />
                <div>
                    <button type="submit">Create</button>
                </div>
            </form>
            <div>
                <h1>Back to Login</h1>
                <button onClick={handleLoginNav}>Login</button>
            </div>
        </div>
    );
}

export default Register;
