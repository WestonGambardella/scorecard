import React, { useEffect, useState } from 'react';
import { useAuthContext } from '../../Context/AuthContext';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { setToken } from '../../Slices/authSlice'


function Login() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const { login } = useAuthContext();
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const token = useSelector((state) => state.auth.token)

    useEffect(()=> {
        if (token !== null){
            navigate('/')
        }
    }, [token, navigate])

    const handleLogin = async (event) => {
        event.preventDefault();
        const loginReponse = await login(username, password);
        dispatch(setToken(loginReponse))
        navigate('/')
    };

    const handleUsernameChange = (event) => {
        setUsername(event.target.value);

    };

    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    };

    const handleRegisterNav = () => {
        navigate('/register')
    }

    return (
        <div>
            <form onSubmit={handleLogin}>
                <input
                    type="text"
                    placeholder="Username"
                    autoComplete='username'
                    value={username}
                    onChange={handleUsernameChange}
                />
                <input
                    type="password"
                    placeholder="Password"
                    autoComplete='current-password'
                    value={password}
                    onChange={handlePasswordChange}
                />
                <div>
                    <button type="submit">LOGIN</button>
                </div>
            </form>
            <div>
                <h1>Don't have an account?</h1>
                <button onClick={handleRegisterNav}>Create Account</button>
            </div>
        </div>
    );
}

export default Login;
