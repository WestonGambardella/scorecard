import React from 'react';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuthContext } from '../../Context/AuthContext';
import { useSelector, useDispatch } from 'react-redux';
import { deleteToken } from '../../Slices/authSlice'



function HomePage() {
    const { logout } = useAuthContext();
    const token = useSelector((state) => state.auth.token)
    const navigate = useNavigate()
    const dispatch = useDispatch()

    useEffect(()=> {
        if (token === null){
            navigate('/login')
        }
    }, [token, navigate])

    const handleLogout = async () => {
        const logoutResponse = await logout()
        if (logoutResponse === true) {
            dispatch(deleteToken())
        }
    };


  return (
    <div>
      <h1>Welcome to My React App</h1>
      <p>This is a simple homepage for my React app.</p>
      <p>Feel free to explore!</p>
      <button onClick={handleLogout}>LOGOUT</button>
    </div>
  );
}

export default HomePage;