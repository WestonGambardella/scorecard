import { createSlice } from '@reduxjs/toolkit';

export const authSlice = createSlice({
    name: 'auth',
    initialState: {
        token: null,
        token_type: null
    },
    reducers: {
        setToken: (state, action) => {
            state.token = action.payload['access_token'];
            state.token_type = action.payload['token_type']
        },
        deleteToken: (state) => {
            state.token = null; 
        } 
    }
});

export const { setToken, deleteToken } = authSlice.actions;
export default authSlice.reducer;