import React, { createContext, useContext } from "react";

const AuthContext = createContext();

export const AuthProvider = (props) => {
    const { children, baseUrl } = props;


  const logout = async () => {
      const url = `${baseUrl}/token`;
      fetch(url, { method: "delete", credentials: "include" })
      return true
    };

  const login = async (username, password) => {
    const url = `${baseUrl}/token`;
    const form = new FormData();
    form.append("username", username);
    form.append("password", password);

    try {
      const response = await fetch(url, {
        method: "post",
        credentials: "include",
        body: form,
      });
      if (!response.ok) {
        throw new Error(`Login failed with status: ${response.status}`);
      }
      const responseData = await response.json();
      return responseData
    } catch (error) {
      console.error("Login error:", error);
    }
  };


  const register = async (userData) => {
    const url = `${baseUrl}/api/accounts`;
    const method = "POST";
    fetch(url, {
      method,
      body: JSON.stringify(userData),
      headers: {
        "Content-Type": "application/json",
      },
    })
    .catch(console.error);
    return true
    };


  return (
    <AuthContext.Provider value={{ login, logout, register }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuthContext = () => useContext(AuthContext);