
import { AuthProvider } from './Context/AuthContext';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import './App.css';
import HomePage from './pages/Homepage/HomePage';
import Login from './pages/Authentication/login';
import Register from './pages/Authentication/register';

const REACT_APP_API_HOST = process.env.REACT_APP_API_HOST


function App() {
  return (
    <AuthProvider baseUrl={'http://localhost:8000'}>
      <Router>
       <Routes>
        <Route path='/' element={<HomePage/>}/>
        <Route path='/login' element={<Login/>}/>
        <Route path='/register' element={<Register/>}/>
       </Routes>
      </Router>
    </AuthProvider>
  );
}

export default App;
