import React, { useEffect, useState } from 'react';
import { StyleSheet, SafeAreaView } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { AuthProvider } from './context/AuthContext';
import { Ionicons } from '@expo/vector-icons';
import useToken from './context/AuthContext';
import Home from './screens/home';
import Login from './screens/login';
import Register from './screens/register';
import Profile from './screens/profile';
import CreateScorecard from './screens/createScorecard';
import styles from './screens/styles';
import UpdateProfile from './screens/updateProfile';
import NineHoleScorecard from './screens/NineHoleScorecard';
import EighteenHoleScorecard from './screens/EighteenHoleScorecard';

const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();
const REACT_APP_API_HOST = 'http://localhost:8000'

export default function App() {
  return (
    <AuthProvider baseUrl='http://localhost:8000'>
      <Layout />
    </AuthProvider>
  );
}

export const Layout = () => {
  const { token } = useToken();
  return (
    <SafeAreaView style={SafeAreaStyles.container}>
      <NavigationContainer>
        {token ? (
          <>
          <Tab.Navigator
          screenOptions={({ route }) => ({
            headerShown: false,
            tabBarIcon: ({ focused, color, size }) => {
              let iconName;

              if (route.name === 'Home') {
                iconName = focused ? 'ios-home' : 'ios-home-outline';
              } else if (route.name === 'Profile Stack') {
                iconName = focused ? 'ios-person' : 'ios-person-outline';
              } else if (route.name === 'New Scorecard') {
                iconName = focused ? 'golf' : 'golf-outline';
              }
              const iconSize = focused ? size + 15 : size + 10
              return <Ionicons name={iconName} size={iconSize} color={color} style={styles.icon}/>;
            },
            tabBarActiveTintColor: '#b5592e',
            tabBarInactiveTintColor: '#d7b56b',
            tabBarLabel: "",
            tabBarStyle: {
              backgroundColor: '#333c31',
              borderTopWidth: 1,
              borderTopColor: '#333c31',
              height: 60
            },
          })}
        >
          <Tab.Screen name="Home" component={Home} />
          <Tab.Screen name="Profile Stack" component={ProfileStackScreen}/>
          <Tab.Screen name="New Scorecard" component={ScorecardStackScreen} />
        </Tab.Navigator>
        </>
        ) : (
          <Stack.Navigator> 
            <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
            <Stack.Screen name="Register" component={Register} options={{ headerShown: false }} />
          </Stack.Navigator>
        )}
      </NavigationContainer>
    </SafeAreaView>
  );
};


const ProfileStackScreen = () => (
  <Stack.Navigator>
    <Stack.Screen name="Profile" component={Profile} options={{ headerShown: false }}/>
    <Stack.Screen name="Update" component={UpdateProfile} options={{ headerShown: false }}/>
  </Stack.Navigator>
);


const ScorecardStackScreen = () => (
  <Stack.Navigator>
    <Stack.Screen name="CreateScorecard" component={CreateScorecard} options={{ headerShown: false }}/>
    <Stack.Screen name="NineHoleScorecard" component={NineHoleScorecard} options={{ headerShown: false }}/>
    <Stack.Screen name="EighteenHoleScorecard" component={EighteenHoleScorecard} options={{ headerShown: false }}/>
  </Stack.Navigator>
);





const SafeAreaStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#333c31',
  },
});