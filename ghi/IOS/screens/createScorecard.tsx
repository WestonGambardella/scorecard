import { View, Text, Image, Pressable } from 'react-native'
import React from 'react'
import styles from './styles'

const CreateScorecard = ({ navigation }) => {
    return (
        <>
        <View style={styles.scorecardsafearea}>
            <Text style={styles.scorecardbanner}>Create Scorecard</Text>
        </View>
        <View style={styles.scorecardbackgroundimagecontainer}>
            <Image source={(require("../assets/scorecard-background-image.jpg"))} 
            style={styles.scorecardstretchedimage} 
            />
        </View>
        <View style={styles.scorecardchoicewindow}>
            <Pressable 
            style={styles.leftpane}
            onPress={() => navigation.navigate('NineHoleScorecard')}
            >
                <Text style={styles.leftpanetext}> 9 Holes</Text>
            </Pressable>
            <Pressable 
            style={styles.rightpane}
            onPress={() => navigation.navigate('EighteenHoleScorecard')}
            >
                <Text style={styles.rightpanetext}> 18 Holes</Text>
            </Pressable>
        </View>
        </>
    )
}

export default CreateScorecard
