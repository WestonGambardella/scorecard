import { View, TextInput, Text, Pressable, ActivityIndicator, Image, Modal } from 'react-native'
import React, { useEffect, useState } from 'react'
import * as ImagePicker from 'expo-image-picker';
import  useToken from '../context/AuthContext'
import styles from './styles'
import * as MailComposer from 'expo-mail-composer';


const Register = ({ navigation }) => {

    const [loading, setLoading] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");
    const [first_name, setFirstName] = useState("")
    const [last_name, setLastName] = useState("")
    const [email, setEmail] = useState("")
    const [homecourse, setHomeCourse] = useState("")
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [passwordcheck, setPasswordCheck] = useState("")
    const [profilePicture, setProfilePicture] = useState(null)
    const [thumbnailVisible, setThumbnailVisible] = useState(false);
    const [uploadedImageUrl, setUploadedImageUrl] = useState(null);
    const [firstVerificationNum, setFirstVerificationNum] = useState(0)
    const [secondVerificationNum, setSecondVerificationNum] = useState(0)
    const [thirdVerificationNum, setThirdVerificationNum] = useState(0)
    const [fourthVerificationNum, setFourthVerificationNum] = useState(0)
    const [isModalVisible, setModalVisible] = useState(false)
    const [modalFirstNum, setModalFirstNum] = useState(0)
    const [modalSecondNum, setModalSecondNum] = useState(0)
    const [modalThirdNum, setModalThirdNum] = useState(0)
    const [modalFourthNum, setModalFourthNum] = useState(0)
    const { register } = useToken()


    const toggleThumbnailVisible = () => {
      setThumbnailVisible(!thumbnailVisible);
    };

    const generateRandomNumber = () => {
      const min = 1;
      const max = 9;
      const randomNumber = Math.floor(Math.random() * (max - min + 1)) + min;
      return randomNumber;
    };


    const sendEmail = async () => {
      const firstNum = generateRandomNumber();
      const secondNum = generateRandomNumber();
      const thirdNum = generateRandomNumber();
      const fourthNum = generateRandomNumber();
    
      const emailContent = `Hey ${first_name}! Here's your confirmation code: ${firstNum} + ${secondNum} + ${thirdNum} + ${fourthNum}`;
    
      try {
        const result = await MailComposer.composeAsync({
          recipients: [email],
          subject: 'Scorecard Email Confirmation',
          body: emailContent,
          isHtml: false,
        });
    
        if (result.status === 'sent') {
          console.log('Email sent successfully');
        } else {
          console.log('Email not sent');
        }
      } catch (error) {
        console.error('Error sending email:', error);
      }
    };


    const toggleModal = () => {
      setModalVisible(!isModalVisible);
    };

    const toggleModalAndSendEmail = () => {
      setModalVisible(!isModalVisible);
      const firstNum = generateRandomNumber()
      setFirstVerificationNum(firstNum)
      const secondNum = generateRandomNumber()
      setSecondVerificationNum(secondNum)
      const thirdNum = generateRandomNumber()
      setThirdVerificationNum(thirdNum)
      const fourthNum = generateRandomNumber()
      setFourthVerificationNum(fourthNum)
      sendEmail()
    };


    const handleSubmit = async () => {
        try {
          if (!username || !password) {
            setErrorMessage('Please fill out all fields.');
          } else if (password !== passwordcheck) {
            setErrorMessage('Passwords do not match');
          } else if (profilePicture === null){
            setErrorMessage('Please choose Profile Picture');
          } else {
            const userInputs = {
              "first_name": first_name,
              "last_name": last_name,
              "email": email,
              "username": username,
              "password": password,
              "home_course": homecourse,
              "profile_pic_url": uploadedImageUrl
            };
            const response = await register(userInputs);
          }
        } catch (error) {
          setErrorMessage('An error occurred during registration');
        }
      };

      async function uploadImage(profilePicture) {
        const uri = profilePicture.assets[0]?.uri
        const fileName = uri.split('/').pop();
        const formData = new FormData();
        formData.append('file', {
          uri: profilePicture.assets[0]?.uri,
          type: profilePicture.assets[0]?.type,
          name: fileName
        });
        try {
          const response = await fetch('http://localhost:8000/api/upload-image', {
            method: 'POST',
            body: formData,
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          });
    
          if (response.ok) {
            const data = await response.json();
            setUploadedImageUrl(data);
          } else {
            setErrorMessage('Error uploading image');
          }
        } catch (error) {
          setErrorMessage('An error occurred during image upload');
        }
      };
        

      const pickImage = async () => {
        const result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
          allowsEditing: true,
          aspect: [4, 3],
          quality: 1,
        });
    
        if (!result.canceled) {
          setProfilePicture(result);
          toggleThumbnailVisible()
          uploadImage(result)
        }
      };
      
        
      const pickImageAgain = async () => {
        const result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
          allowsEditing: true,
          aspect: [4, 3],
          quality: 1,
        });
    
        if (!result.cancelled) {
          setProfilePicture(result);
        }
      };

    const handleFirstnameChange = (text) => {
        setFirstName(text);
        setErrorMessage('');
      };
    
    const handleLastnameChange = (text) => {
        setLastName(text);
        setErrorMessage('');
      };
    
    const handleEmailChange = (text) => {
        setEmail(text);
        setErrorMessage('');
      };

    const handleUsernameChange = (text) => {
        setUsername(text);
        setErrorMessage('');
      };
    
    const handleHomecourseChange = (text) => {
        setHomeCourse(text);
        setErrorMessage('');
      };

    const handlePasswordChange = (text) => {
        setPassword(text);
        setErrorMessage('');
      };

    const handlePasswordCheckChange = (text) => {
        setPasswordCheck(text);
        setErrorMessage('');
      };

    const handleFirstNumberInput = (value) => {
      setModalFirstNum(value)
    }
    const handleSecondNumberInput = (value) => {
      setModalSecondNum(value)
    }
    const handleThirdNumberInput = (value) => {
      setModalThirdNum(value)
    }
    const handleFourthNumberInput = (value) => {
      setModalFourthNum(value)
    }

    return (
        <View style={styles.container}>
          {isModalVisible && (
            <Modal
              animationType="slide"
              transparent={true}
              visible={isModalVisible}
            >
              <View style={styles.confirmmodalcont}>
                <View style={styles.confirmmodal}>
                  <Text>Email Authentication</Text>
                  <Text>We sent an email to {email}, please Input the verification code recieved in your email</Text>
                  <View style={styles.emailauthcont}>
                    <TextInput
                      style={{ height: 40, borderColor: 'gray', borderWidth: 1, marginBottom: 10, padding: 10 }}
                      placeholder="0"
                      keyboardType="numeric"
                      onChangeText={handleFirstNumberInput}
                    />
                    <Text> - </Text>
                    <TextInput
                    style={{ height: 40, borderColor: 'gray', borderWidth: 1, marginBottom: 10, padding: 10 }}
                    placeholder="0"
                    keyboardType="numeric"
                    onChangeText={handleSecondNumberInput}
                    />
                    <Text> - </Text>
                    <TextInput
                    style={{ height: 40, borderColor: 'gray', borderWidth: 1, marginBottom: 10, padding: 10 }}
                    placeholder="0"
                    keyboardType="numeric"
                    onChangeText={handleThirdNumberInput}
                    />
                    <Text> - </Text>
                    <TextInput
                    style={{ height: 40, borderColor: 'gray', borderWidth: 1, marginBottom: 10, padding: 10 }}
                    placeholder="0"
                    keyboardType="numeric"
                    onChangeText={handleFourthNumberInput}
                    />
                  </View>
                  <View style={styles.emailauthconfirm}>
                    <Pressable style={styles.button}>
                      <Text>Submit</Text>
                    </Pressable>
                    <Pressable style={styles.button} onPress={toggleModal}>
                      <Text>Go Back</Text>
                    </Pressable>
                  </View>
                </View>
              </View>
            </Modal>
          )}
            <View style={styles.inputContainer}>
            {!thumbnailVisible && (
              <View style={styles.profileimageContainer}>
                <Image source={require('ghi/assets/default_profile_img.jpg')} style={styles.defaultimg}/>
                  <Pressable onPress={pickImage} style={styles.profilepicbutton}>
                  <Text style={styles.profilebuttontext}>CHOOSE PROFILE PICTURE</Text>
                </Pressable>
                </View>
                )}
                {thumbnailVisible && profilePicture && (
                <View style={styles.profileimageContainer}>
                  <Image
                    source={{ uri: profilePicture.assets[0].uri }}
                    style={styles.thumbnail}
                  />
                  <Pressable onPress={pickImageAgain} style={styles.profilepicbutton}>
                  <Text style={styles.profilebuttontext}>CHOOSE ANOTHER IMAGE</Text>
                </Pressable>
                </View>
        )}
                <TextInput
                style={styles.regtextInput}
                placeholder="First Name" 
                onChangeText={handleFirstnameChange}
                />
                <TextInput
                style={styles.regtextInput}
                placeholder="Last Name" 
                onChangeText={handleLastnameChange}
                />
                <TextInput
                autoCapitalize='none'
                style={styles.regtextInput}
                placeholder="Email" 
                onChangeText={handleEmailChange}
                />
                <TextInput
                autoCapitalize='none'
                style={styles.regtextInput}
                placeholder="Username" 
                onChangeText={handleUsernameChange}
                />
                <TextInput
                style={styles.regtextInput}
                placeholder="Homecourse" 
                onChangeText={handleHomecourseChange}
                />
                <TextInput
                autoCapitalize='none'
                style={styles.regtextInput}
                placeholder="Password" 
                secureTextEntry={true} 
                onChangeText={handlePasswordChange} 
                value={password} 
                />
                <TextInput
                autoCapitalize='none'
                style={styles.regtextInput}
                placeholder="Confirm Password" 
                secureTextEntry={true} 
                onChangeText={handlePasswordCheckChange} 
                value={passwordcheck} 
                />
                <View style={styles.buttoncontainer}>
                    {errorMessage ? (
                        <View style={styles.errorcontainer}>
                        <Text style={styles.errorText}>{errorMessage}</Text>
                        </View>
                    ) : null}
                    <Pressable
                        onPress={handleSubmit}
                        style={styles.emailbutton}
                        >
                        <Text style={styles.buttontext}>Register</Text>
                    </Pressable>
                    <Pressable 
                        onPress={() => navigation.navigate('Login')} 
                        style={styles.longbutton1}
                        >
                        <Text style={styles.buttontext}>CANCEL</Text>    
                    </Pressable>
                </View>
            </View>
        </View>
    )
}

export default Register
