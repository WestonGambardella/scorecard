import qs from 'qs';
import { Linking } from 'react-native';


export async function sendEmail(to: string, subject: string, body: string) {
    try {
        let url = `mailto:${to}`;

        const query = qs.stringify({
            subject: subject,
            body: body,
        });

        if (query.length) {
            url += `?${query}`;
        }

        console.log('Email URL:', url);

        const canOpen = await Linking.canOpenURL(url);

        if (!canOpen) {
            throw new Error('Provided URL can not be handled');
        }

        return Linking.openURL(url);
    } catch (error) {
        console.error('Error sending email:', error);
        throw error;
    }
}