import { View, Pressable, TextInput, Text, Image, SafeAreaView } from 'react-native'
import React, { useState } from 'react'
import  useToken from '../context/AuthContext'
import styles from './styles';

function Login({ navigation }) {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [errorMessage, setErrorMessage] = useState('');
    const [missingCredentials, setMissingCredentials] = useState(false)
    const { login } = useToken()
    const token = useToken()


    const loginUser = async () => {
        if (!username || !password) {
            setErrorMessage('Please fill out all fields.');
          } else {
            login(username, password)
            setErrorMessage('');
            if (token.token === null) {
                setErrorMessage("Incorrect Username or Password")
                setMissingCredentials(true)
            }
          }
    }


    const handleUsernameChange = (text) => {
        setUsername(text);
        setErrorMessage('');
      };

    const handlePasswordChange = (text) => {
    setPassword(text);
    setErrorMessage('');
    };

    return (
        <View style={styles.container}>
            <Image
                source={require('ghi/assets/logo.png')}
                style={styles.imageContainer}
                />
            <View style={styles.inputContainer}>
                <TextInput
                autoCapitalize='none'
                style={styles.textInput} 
                placeholder="Username" 
                onChangeText={handleUsernameChange} 
                value={username} 
                />
                <TextInput
                autoCapitalize='none'
                placeholder="Password"
                style={styles.textInput}
                secureTextEntry={true} 
                onChangeText={handlePasswordChange} 
                value={password} 
                />
                <View style={styles.loginfielderror}>
                        <Pressable 
                        onPress={loginUser} 
                        style={styles.button}
                        >
                        <Text style={styles.buttontext}>LOGIN</Text>    
                        </Pressable>
                </View>
                {errorMessage ? (
                    <View style={styles.errorlogin}>
                        <Text style={styles.errorText}>{errorMessage}</Text>
                    </View>
                    ) : null}
                <View style={styles.registercontainer}>
                    {/* <View style={styles.passwordresetcontainer}>
                        <View style={styles.forgotcreds}>
                            <Pressable style={styles.forgotbutton}>
                                <Text style={styles.forgottext}>Forget Your Password?</Text>
                            </Pressable>
                        </View>
                        <View style={styles.forgotcreds}>
                            <Pressable style={styles.forgotbutton}>
                                <Text style={styles.forgottext}>Forget Your Username?</Text>
                            </Pressable>
                        </View>
                    </View> */}
                    <Text style={styles.accounttext}>Don't have an account?</Text>
                    <Pressable
                    onPress={() => navigation.navigate('Register')}
                    style={styles.accountbutton}
                    >
                    <Text style={styles.accountbuttontext}>CREATE ACCOUNT</Text>    
                    </Pressable>
                </View>
            </View>
        </View>
    )
}


export default Login
