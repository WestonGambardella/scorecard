import { View, Text, Pressable, TextInput, FlatList, StyleSheet, Dimensions, Modal } from 'react-native'
import React, { useState, useRef } from 'react'
import { MaterialIcons } from '@expo/vector-icons';
import styles from './styles'
import RNPickerSelect from 'react-native-picker-select';
import useToken from '../context/AuthContext';

const NineHoleScorecard = ({ navigation }) => {
    const { token } = useToken()
    const REACT_APP_API_HOST = 'http://localhost:8000'
    const [isLoading, setIsLoading] = useState(false)
    const [courseState, setSelectedState] = useState(null);
    const [cancel, setCancel] = useState(false)
    const [courseName, setCourseName] = useState("")
    const [courseCity, setCourseCity] = useState("")
    const [courseTee, setCourseTee] = useState("")
    const [avgPutts, setAvgPutts] = useState(0)
    const [totalPutts, setTotalPutts] = useState(0)
    const [avgFir, setAvgFir] = useState(0.0)
    const [avgGir, setAvgGir] = useState(0.0)
    const [score, setScore] = useState(0)
    const [parDifferential, setParDifferential] = useState(0)

    const [hole1par, setHole1Par] = useState(0)
    const [hole1putts, setHole1Putts] = useState(0)
    const [hole1score, setHole1Score] = useState(0)
    const [is1FirChecked, set1FirChecked] = useState(false);
    const [is1GirChecked, set1GirChecked] = useState(false);

    const [hole2par, setHole2Par] = useState(0)
    const [hole2putts, setHole2Putts] = useState(0)
    const [hole2score, setHole2Score] = useState(0)
    const [is2FirChecked, set2FirChecked] = useState(false);
    const [is2GirChecked, set2GirChecked] = useState(false);

    const [hole3par, setHole3Par] = useState(0)
    const [hole3putts, setHole3Putts] = useState(0)
    const [hole3score, setHole3Score] = useState(0)
    const [is3FirChecked, set3FirChecked] = useState(false);
    const [is3GirChecked, set3GirChecked] = useState(false);

    const [hole4par, setHole4Par] = useState(0)
    const [hole4putts, setHole4Putts] = useState(0)
    const [hole4score, setHole4Score] = useState(0)
    const [is4FirChecked, set4FirChecked] = useState(false);
    const [is4GirChecked, set4GirChecked] = useState(false);

    const [hole5par, setHole5Par] = useState(0)
    const [hole5putts, setHole5Putts] = useState(0)
    const [hole5score, setHole5Score] = useState(0)
    const [is5FirChecked, set5FirChecked] = useState(false);
    const [is5GirChecked, set5GirChecked] = useState(false);

    const [hole6par, setHole6Par] = useState(0)
    const [hole6putts, setHole6Putts] = useState(0)
    const [hole6score, setHole6Score] = useState(0)
    const [is6FirChecked, set6FirChecked] = useState(false);
    const [is6GirChecked, set6GirChecked] = useState(false);

    const [hole7par, setHole7Par] = useState(0)
    const [hole7putts, setHole7Putts] = useState(0)
    const [hole7score, setHole7Score] = useState(0)
    const [is7FirChecked, set7FirChecked] = useState(false);
    const [is7GirChecked, set7GirChecked] = useState(false);

    const [hole8par, setHole8Par] = useState(0)
    const [hole8putts, setHole8Putts] = useState(0)
    const [hole8score, setHole8Score] = useState(0)
    const [is8FirChecked, set8FirChecked] = useState(false);
    const [is8GirChecked, set8GirChecked] = useState(false);

    const [hole9par, setHole9Par] = useState(0)
    const [hole9putts, setHole9Putts] = useState(0)
    const [hole9score, setHole9Score] = useState(0)
    const [is9FirChecked, set9FirChecked] = useState(false);
    const [is9GirChecked, set9GirChecked] = useState(false);

    const flatListRef = useRef(null);

    const stateOptions = [
        { label: 'Alabama', value: 'AL' },
        { label: 'Alaska', value: 'AK' },
        { label: 'Arizona', value: 'AZ' },
        { label: 'Arkansas', value: 'AR' },
        { label: 'California', value: 'CA' },
        { label: 'Colorado', value: 'CO' },
        { label: 'Connecticut', value: 'CT' },
        { label: 'Delaware', value: 'DE' },
        { label: 'Florida', value: 'FL' },
        { label: 'Georgia', value: 'GA' },
        { label: 'Hawaii', value: 'HI' },
        { label: 'Idaho', value: 'ID' },
        { label: 'Illinois', value: 'IL' },
        { label: 'Indiana', value: 'IN' },
        { label: 'Iowa', value: 'IA' },
        { label: 'Kansas', value: 'KS' },
        { label: 'Kentucky', value: 'KY' },
        { label: 'Louisiana', value: 'LA' },
        { label: 'Maine', value: 'ME' },
        { label: 'Maryland', value: 'MD' },
        { label: 'Massachusetts', value: 'MA' },
        { label: 'Michigan', value: 'MI' },
        { label: 'Minnesota', value: 'MN' },
        { label: 'Mississippi', value: 'MS' },
        { label: 'Missouri', value: 'MO' },
        { label: 'Montana', value: 'MT' },
        { label: 'Nebraska', value: 'NE' },
        { label: 'Nevada', value: 'NV' },
        { label: 'New Hampshire', value: 'NH' },
        { label: 'New Jersey', value: 'NJ' },
        { label: 'New Mexico', value: 'NM' },
        { label: 'New York', value: 'NY' },
        { label: 'North Carolina', value: 'NC' },
        { label: 'North Dakota', value: 'ND' },
        { label: 'Ohio', value: 'OH' },
        { label: 'Oklahoma', value: 'OK' },
        { label: 'Oregon', value: 'OR' },
        { label: 'Pennsylvania', value: 'PA' },
        { label: 'Rhode Island', value: 'RI' },
        { label: 'South Carolina', value: 'SC' },
        { label: 'South Dakota', value: 'SD' },
        { label: 'Tennessee', value: 'TN' },
        { label: 'Texas', value: 'TX' },
        { label: 'Utah', value: 'UT' },
        { label: 'Vermont', value: 'VT' },
        { label: 'Virginia', value: 'VA' },
        { label: 'Washington', value: 'WA' },
        { label: 'West Virginia', value: 'WV' },
        { label: 'Wisconsin', value: 'WI' },
        { label: 'Wyoming', value: 'WY' },
      ];

      function roundToTenth(number) {
        return Math.round(number * 10) / 10;
      }


      const handleSubmit = async () => {
        setIsLoading(true)
        const RoundInfo = {
            "course_name": courseName,
            "course_city": courseCity,
            "course_state": courseState,
            "tees": courseTee,
            "par_differential": parDifferential,
            "avg_putts": avgPutts,
            "avg_gir": (avgGir/9),
            "avg_fir": (avgFir/9),
            "score": score
          };
          const url = `${REACT_APP_API_HOST}/api/create_ninescorecard`;
            const response = await fetch(url,{
                method: "POST",
                body: JSON.stringify(RoundInfo),
                headers: {
                    Authorization: `Bearer ${token}`,
                    "Content-Type": "application/json",
                },
            })
            if (response.status === 200) {

                navigation.navigate('CreateScorecard')
              } else {
                setIsLoading(false)
              }
          } 

      const handleCancel = () => {
        setCancel(true)
      };
      const hideModal = () => {
        setCancel(false);
      };

      const handleConfirmedCancel = () => {
        navigation.navigate('CreateScorecard')
      };
      const scrollToIndex = (index) => {
        flatListRef.current.scrollToIndex({ index, animated: true });
      };

      const handleCourseName = (text) => {
        setCourseName(text);
      };
      const handleCourseCity = (text) => {
        setCourseCity(text);
      };
      const handleCourseTee = (text) => {
        setCourseTee(text);
      };

      //hole 1 functions start ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    const handleHole1Par = (text) => {
        const intText = parseInt(text)
        setHole1Par(intText)
    }
    const handleHole1Putts = (text) => {
        const intText = parseInt(text)
        setHole1Putts(intText)
    }
    const handleHole1Score = (text) => {
        const intText = parseInt(text)
        setHole1Score(intText)
    }
    const handle1FirPress = () => {
        set1FirChecked(!is1FirChecked);
      };
    const handle1GirPress = () => {
        set1GirChecked(!is1GirChecked);
      };
      const hole1ScrollDownToIndex = (index) => {
        flatListRef.current.scrollToIndex({ index, animated: true });
        setParDifferential(hole1score - hole1par)
        setTotalPutts(hole1putts)
        setAvgPutts(hole1putts)
        setScore(score+hole1score)
        if(is1FirChecked){
            setAvgFir(1)
        }
        if(is1GirChecked){
            setAvgGir(1)
        }
      };
      //hole 1 functions end ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

      //hole 2 functions start ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    const handleHole2Par = (text) => {
        const intText = parseInt(text)
        setHole2Par(intText)
    }
    const handleHole2Putts = (text) => {
        const intText = parseInt(text)
        setHole2Putts(intText)
    }
    const handleHole2Score = (text) => {
        const intText = parseInt(text)
        setHole2Score(intText)
    }
    const handle2FirPress = () => {
        set2FirChecked(!is2FirChecked);
      };
    const handle2GirPress = () => {
        set2GirChecked(!is2GirChecked);
      };
      const hole2ScrollDownToIndex = (index) => {
        flatListRef.current.scrollToIndex({ index, animated: true });
        setParDifferential((hole2score - hole2par)+parDifferential)
        const newTotal = totalPutts + hole2putts
        setTotalPutts(newTotal)
        console.log("total putts hole2: ", newTotal)
        setAvgPutts(newTotal/2)
        setScore(score+hole2score)
        if(is2FirChecked){
            setAvgFir(avgFir + 1)
        }
        if(is2GirChecked){
            setAvgGir(avgGir + 1)
        }
      };
      //hole 2 functions end ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

      //hole 3 functions start ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      const handleHole3Par = (text) => {
        const intText = parseInt(text)
        setHole3Par(intText)
    }
    const handleHole3Putts = (text) => {
        const intText = parseInt(text)
        setHole3Putts(intText)
    }
    const handleHole3Score = (text) => {
        const intText = parseInt(text)
        setHole3Score(intText)
    }
    const handle3FirPress = () => {
        set3FirChecked(!is3FirChecked);
      };
    const handle3GirPress = () => {
        set3GirChecked(!is3GirChecked);
      };
      const hole3ScrollDownToIndex = (index) => {
        flatListRef.current.scrollToIndex({ index, animated: true });
        setParDifferential((hole3score - hole3par) + parDifferential)
        const newTotal = totalPutts + hole3putts
        setTotalPutts(newTotal)
        console.log("total putts hole3: ", newTotal)
        
        const avg = roundToTenth(newTotal/3)
        setAvgPutts(avg)
        setScore(score+hole3score)
        if(is3FirChecked){
            setAvgFir(avgFir + 1)
        }
        if(is3GirChecked){
            setAvgGir(avgGir + 1)
        }
      };
      //hole 3 functions end ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

        //hole 4 functions start ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        const handleHole4Par = (text) => {
            const intText = parseInt(text)
            setHole4Par(intText)
        }
        const handleHole4Putts = (text) => {
            const intText = parseInt(text)
            setHole4Putts(intText)
        }
        const handleHole4Score = (text) => {
            const intText = parseInt(text)
            setHole4Score(intText)
        }
        const handle4FirPress = () => {
            set4FirChecked(!is4FirChecked);
            };
        const handle4GirPress = () => {
            set4GirChecked(!is4GirChecked);
            };
            const hole4ScrollDownToIndex = (index) => {
            flatListRef.current.scrollToIndex({ index, animated: true });
            setParDifferential((hole4score - hole4par)+parDifferential)
            const newTotal = totalPutts + hole4putts
            setTotalPutts(newTotal)
            console.log("total putts hole4: ", newTotal)
            
            const avg = roundToTenth(newTotal/4)
            setAvgPutts(avg)
            setScore(score+hole4score)
            if(is4FirChecked){
                setAvgFir(avgFir + 1)
            }
            if(is4GirChecked){
                setAvgGir(avgGir + 1)
            }
            };
            //hole 4 functions end ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

            //hole 5 functions start ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        const handleHole5Par = (text) => {
            const intText = parseInt(text)
            setHole5Par(intText)
        }
        const handleHole5Putts = (text) => {
            const intText = parseInt(text)
            setHole5Putts(intText)
        }
        const handleHole5Score = (text) => {
            const intText = parseInt(text)
            setHole5Score(intText)
        }
        const handle5FirPress = () => {
            set5FirChecked(!is5FirChecked);
            };
        const handle5GirPress = () => {
            set5GirChecked(!is5GirChecked);
            };
            const hole5ScrollDownToIndex = (index) => {
            flatListRef.current.scrollToIndex({ index, animated: true });
            setParDifferential((hole5score - hole5par)+parDifferential)
            const newTotal = totalPutts + hole5putts
            setTotalPutts(newTotal)
            console.log("total putts hole5: ", newTotal)
            
            const avg = roundToTenth(newTotal/5)
            setAvgPutts(avg)
            setScore(score+hole5score)
            if(is5FirChecked){
                setAvgFir(avgFir + 1)
            }
            if(is5GirChecked){
                setAvgGir(avgGir + 1)
            }
            };
            //hole 5 functions end ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

            //hole 6 functions start ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        const handleHole6Par = (text) => {
            const intText = parseInt(text)
            setHole6Par(intText)
        }
        const handleHole6Putts = (text) => {
            const intText = parseInt(text)
            setHole6Putts(intText)
        }
        const handleHole6Score = (text) => {
            const intText = parseInt(text)
            setHole6Score(intText)
        }
        const handle6FirPress = () => {
            set6FirChecked(!is6FirChecked);
            };
        const handle6GirPress = () => {
            set6GirChecked(!is6GirChecked);
            };
            const hole6ScrollDownToIndex = (index) => {
            flatListRef.current.scrollToIndex({ index, animated: true });
            setParDifferential((hole6score - hole6par)+parDifferential)
            const newTotal = totalPutts + hole6putts
            setTotalPutts(newTotal)
            console.log("total putts hole6: ", newTotal)
            
            const avg = roundToTenth(newTotal/6)
            setAvgPutts(avg)
            setScore(score+hole6score)
            if(is6FirChecked){
                setAvgFir(avgFir + 1)
            }
            if(is6GirChecked){
                setAvgGir(avgGir + 1)
            }
            };
            //hole 6 functions end ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

            //hole 7 functions start ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        const handleHole7Par = (text) => {
            const intText = parseInt(text)
            setHole7Par(intText)
        }
        const handleHole7Putts = (text) => {
            const intText = parseInt(text)
            setHole7Putts(intText)
        }
        const handleHole7Score = (text) => {
            const intText = parseInt(text)
            setHole7Score(intText)
        }
        const handle7FirPress = () => {
            set7FirChecked(!is7FirChecked);
            };
        const handle7GirPress = () => {
            set7GirChecked(!is7GirChecked);
            };
            const hole7ScrollDownToIndex = (index) => {
            flatListRef.current.scrollToIndex({ index, animated: true });
            setParDifferential((hole7score - hole7par)+parDifferential)
            const newTotal = totalPutts + hole7putts
            setTotalPutts(newTotal)
            console.log("total putts hole7: ", newTotal)
            
            const avg = roundToTenth(newTotal/7)
            setAvgPutts(avg)
            setScore(score+hole7score)
            if(is7FirChecked){
                setAvgFir(avgFir + 1)
            }
            if(is7GirChecked){
                setAvgGir(avgGir + 1)
            }
            };
            //hole 7 functions end ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

            //hole 8 functions start ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        const handleHole8Par = (text) => {
            const intText = parseInt(text)
            setHole8Par(intText)
        }
        const handleHole8Putts = (text) => {
            const intText = parseInt(text)
            setHole8Putts(intText)
        }
        const handleHole8Score = (text) => {
            const intText = parseInt(text)
            setHole8Score(intText)
        }
        const handle8FirPress = () => {
            set8FirChecked(!is8FirChecked);
            };
        const handle8GirPress = () => {
            set8GirChecked(!is8GirChecked);
            };
            const hole8ScrollDownToIndex = (index) => {
            flatListRef.current.scrollToIndex({ index, animated: true });
            setParDifferential((hole8score - hole8par)+parDifferential)
            const newTotal = totalPutts + hole8putts
            setTotalPutts(newTotal)
            console.log("total putts hole8: ", newTotal)
            
            const avg = roundToTenth(newTotal/8)
            setAvgPutts(avg)
            setScore(score+hole8score)
            if(is8FirChecked){
                setAvgFir(avgFir + 1)
            }
            if(is8GirChecked){
                setAvgGir(avgGir + 1)
            }
            };
            //hole 8 functions end ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

            //hole 9 functions start ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
        const handleHole9Par = (text) => {
            const intText = parseInt(text)
            setHole6Par(intText)
        }
        const handleHole9Putts = (text) => {
            const intText = parseInt(text)
            setHole6Putts(intText)
        }
        const handleHole9Score = (text) => {
            const intText = parseInt(text)
            setHole6Score(intText)
        }
        const handle9FirPress = () => {
            set9FirChecked(!is9FirChecked);
            };
        const handle9GirPress = () => {
            set9GirChecked(!is9GirChecked);
            };
            const hole9ScrollDownToIndex = (index) => {
                flatListRef.current.scrollToIndex({ index, animated: true });
            setParDifferential((hole9score - hole9par)+parDifferential)
            const newTotal = totalPutts + hole9putts
            setTotalPutts(newTotal)
            console.log("total putts hole9: ", newTotal)
            
            const avg = roundToTenth(newTotal/9)
            setAvgPutts(avg)
            setScore(score+hole9score)
            if(is9FirChecked){
                setAvgFir(avgFir + 1)
            }
            if(is9GirChecked){
                setAvgGir(avgGir + 1)
            }
            };
            //hole 9 functions end ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    return (
            <>
                <FlatList
                    ref={flatListRef}
                    data={[
                    { key: 'Course Details' },
                    { key: 'Hole One' },
                    { key: 'Hole Two' },
                    { key: 'Hole Three' },
                    { key: 'Hole Four'},
                    { key: 'Hole Five'},
                    { key: 'Hole Six'},
                    { key: 'Hole Seven'},
                    { key: 'Hole Eight'},
                    { key: 'Hole Nine'},
                    { key: 'Round Stats'}
                    ]}
                    renderItem={({ item, index }) => (
                        <>
                            <View style={styles1.fullScreenItem}>
                            {item.key === 'Course Details' && (
                                <>
                                    <View style={styles1.profileSafeAreaExtend}>
                                        <View style={styles1.headercontainer}>
                                            <Pressable 
                                            style={styles1.cancelroundbutton}
                                            onPress={handleCancel}
                                            >
                                                <Text style={styles1.cancelroundbuttontext}>Cancel Round</Text>
                                            </Pressable>
                                            <View style={styles1.statsheadercontainer}>
                                                <Text style={styles1.statsheader}>Stats</Text>
                                            </View>
                                        </View>
                                        <View style={styles1.statscontainer}>
                                            <View style={styles1.column0}>
                                                <View style={styles1.pardiffcontainer}>
                                                    <View style={styles1.pardiffheader}>
                                                        <Text style={styles1.pardifftext}>Par Differential</Text>
                                                    </View>
                                                    <View style={styles1.pardiffnumbercontainer}>
                                                    {parDifferential > 0 && (
                                                        <Text style={styles1.pardiffnumber}>+ {parDifferential}</Text>
                                                    )}
                                                    {parDifferential <= 0 && (
                                                        <Text style={styles1.pardiffnumber}>{parDifferential}</Text>
                                                    )}
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={styles1.column}>
                                                <Text style={styles1.columnText}>PPH:</Text>
                                                <Text style={styles1.columnText}>Score:</Text>
                                                <Text style={styles1.columnText}>FIR:</Text>
                                                <Text style={styles1.columnText}>GIR:</Text>
                                            </View>
                                            <View style={styles1.column1}>
                                                <Text style={styles1.columnscore}>{avgPutts}</Text>
                                                <Text style={styles1.columnscore}>{score}</Text>
                                                <Text style={styles1.columnscore}>{avgFir}/9</Text>
                                                <Text style={styles1.columnscore}>{avgGir}/9</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles1.courseinfocontainer}>
                                        {cancel && (
                                            <Modal
                                            animationType="slide"
                                            transparent={true}
                                            visible={cancel}
                                            onRequestClose={hideModal}
                                            >
                                                <View style={styles1.cancelpromptcontainer}>
                                                    <Text style={styles1.cancelheader}>Do you want to cancel your round?</Text>
                                                    <Text style={styles1.canceldetail}>All scores for this round will be erased.</Text>
                                                    <Pressable 
                                                    style={styles1.cancelconfirmbutton}
                                                    onPress={handleConfirmedCancel}
                                                    >
                                                        <Text style={styles1.cancelconfirmtext}>Yes</Text>
                                                    </Pressable>
                                                    <Pressable 
                                                    style={styles1.cancelconfirmbutton}
                                                    onPress={hideModal}
                                                    >
                                                        <Text style={styles1.cancelconfirmtext}>No</Text>
                                                    </Pressable>
                                                </View>
                                            </Modal>
                                        )}
                                        <TextInput
                                            style={styles1.regtextInput}
                                            placeholderTextColor="white"
                                            placeholder="Course Name" 
                                            onChangeText={handleCourseName}
                                        />
                                        <TextInput
                                            style={styles1.regtextInput}
                                            placeholderTextColor="white"
                                            placeholder="City/Town"
                                            onChangeText={handleCourseCity}
                                        />
                                        <RNPickerSelect
                                            onValueChange={(value) => setSelectedState(value)}
                                            items={stateOptions}
                                            placeholder={{ label: 'Select a State', value: null}}
                                            value={courseState}
                                            style={{
                                                inputIOS: {
                                                    height: 70,
                                                    borderColor: 'white',
                                                    backgroundColor: '#333c31',
                                                    borderWidth: 5,
                                                    width: '90%',
                                                    fontSize: 40,
                                                    color: 'white',
                                                    borderRadius: 50,
                                                    padding: '5%',
                                                    marginLeft: 20,
                                                    margin: 10,
                                                }
                                            }}
                                        />
                                    <TextInput
                                        style={styles1.regtextInput}
                                        placeholder="Tees"
                                        placeholderTextColor="white"
                                        onChangeText={handleCourseTee}
                                    />
                                    <View style={styles1.scrollcontainer}>
                                        <Pressable 
                                        onPress={() => scrollToIndex(index + 1)}
                                        style={styles1.scrollbutton}
                                        >
                                            <MaterialIcons name="keyboard-arrow-down" size={40} color="black" />
                                        </Pressable>
                                    </View>
                            </View>
                        </>
                        )}

                        {item.key.startsWith('Hole One') && (
                            // this is the start of hole 1 stuff, the entire hole1 input screen
                            <>
                            <View style={styles1.profileSafeAreaExtend}>
                                        <View style={styles1.headercontainer}>
                                            <Pressable 
                                            style={styles1.cancelroundbutton}
                                            onPress={handleCancel}
                                            >
                                                <Text style={styles1.cancelroundbuttontext}>Cancel Round</Text>
                                            </Pressable>
                                            <View style={styles1.statsheadercontainer}>
                                                <Text style={styles1.statsheader}>Stats</Text>
                                            </View>
                                        </View>
                                        <View style={styles1.statscontainer}>
                                            <View style={styles1.column0}>
                                                <View style={styles1.pardiffcontainer}>
                                                    <View style={styles1.pardiffheader}>
                                                        <Text style={styles1.pardifftext}>Par Differential</Text>
                                                    </View>
                                                    <View style={styles1.pardiffnumbercontainer}>
                                                    {parDifferential > 0 && (
                                                        <Text style={styles1.pardiffnumber}>+ {parDifferential}</Text>
                                                    )}
                                                    {parDifferential <= 0 && (
                                                        <Text style={styles1.pardiffnumber}>{parDifferential}</Text>
                                                    )}
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={styles1.column}>
                                                <Text style={styles1.columnText}>PPH:</Text>
                                                <Text style={styles1.columnText}>Score:</Text>
                                                <Text style={styles1.columnText}>FIR Avg:</Text>
                                                <Text style={styles1.columnText}>GIR Avg:</Text>
                                            </View>
                                            <View style={styles1.column1}>
                                                <Text style={styles1.columnscore}>{avgPutts}</Text>
                                                <Text style={styles1.columnscore}>{score}</Text>
                                                <Text style={styles1.columnscore}>{avgFir}/9</Text>
                                                <Text style={styles1.columnscore}>{avgGir}/9</Text>
                                            </View>
                                        </View>
                                    </View>
                                <View style={styles1.holeinfocontainer}>
                                    <View style={styles1.holeinputcontainer}>
                                        <View style={styles1.toprowcontainer}>
                                            <View style={styles1.parinputcontainer}>
                                                <View style={styles1.parintputheader}>
                                                        <Text style={styles1.puttstext}>Hole Par</Text>
                                                </View>
                                                    <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole1Par}
                                                    />
                                            </View>
                                            <View style={styles1.girfircontainer}>
                                                <View style={styles1.firinputcontainer}>
                                                    <View style={styles1.firintputheader}>
                                                            <Text style={styles1.puttstext}>FIR</Text>
                                                    </View>
                                                    <View style={styles1.fircheckbox}>
                                                        <Pressable onPress={handle1FirPress} style={styles1.fircheckboxstyle}>
                                                            <MaterialIcons
                                                            name={is1FirChecked ? 'check-box' : 'check-box-outline-blank'}
                                                            size={70}
                                                            color="white"
                                                            />
                                                        </Pressable>
                                                        <Text>{is1FirChecked ? 'Checked' : 'Unchecked'}</Text>
                                                    </View>
                                                </View>
                                                <View style={styles1.girinputcontainer}>
                                                    <View style={styles1.girintputheader}>
                                                            <Text style={styles1.puttstext}>GIR</Text>
                                                    </View>
                                                    <View style={styles1.fircheckbox}>
                                                        <Pressable onPress={handle1GirPress} style={styles1.fircheckboxstyle}>
                                                            <MaterialIcons
                                                            name={is1GirChecked ? 'check-box' : 'check-box-outline-blank'}
                                                            size={70}
                                                            color="white"
                                                            />
                                                        </Pressable>
                                                        <Text>{is1GirChecked ? 'Checked' : 'Unchecked'}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={styles1.holenumbercontainer}>
                                                <Text style={styles1.holenumbertext}>Hole #1</Text>
                                            </View>
                                        </View>
                                        <View style={styles1.bottomrowcontainer}>
                                            <View style={styles1.puttscontainer}>
                                                <View style={styles1.puttsheader}>
                                                    <Text style={styles1.puttstext}>Putts</Text>
                                                </View>
                                                <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole1Putts}
                                                    />
                                            </View>
                                            <View style={styles1.scorecontainter}>
                                                <View style={styles1.scoreheader}>
                                                    <Text style={styles1.puttstext}>Score</Text>
                                                </View>
                                                <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole1Score}
                                                    />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles1.holescrollcontainer}>
                                        <View style={styles1.horizontalContainer}>
                                            <Pressable
                                            onPress={() => hole1ScrollDownToIndex(index + 1)}
                                            style={styles1.holescrollbuttonright}
                                            >
                                                <MaterialIcons name="keyboard-arrow-down" size={40} color="black" />
                                            </Pressable>
                                        </View>
                                    </View>
                                </View>
                            </>
                            // this is the end of the entire hole1screen
                            )}
                            {item.key.startsWith('Hole Two') && (
                            // this is the start of hole 2 stuff, the entire hole2 input screen
                            <>
                            <View style={styles1.profileSafeAreaExtend}>
                                        <View style={styles1.headercontainer}>
                                            <Pressable 
                                            style={styles1.cancelroundbutton}
                                            onPress={handleCancel}
                                            >
                                                <Text style={styles1.cancelroundbuttontext}>Cancel Round</Text>
                                            </Pressable>
                                            <View style={styles1.statsheadercontainer}>
                                                <Text style={styles1.statsheader}>Stats</Text>
                                            </View>
                                        </View>
                                        <View style={styles1.statscontainer}>
                                            <View style={styles1.column0}>
                                                <View style={styles1.pardiffcontainer}>
                                                    <View style={styles1.pardiffheader}>
                                                        <Text style={styles1.pardifftext}>Par Differential</Text>
                                                    </View>
                                                    <View style={styles1.pardiffnumbercontainer}>
                                                    {parDifferential > 0 && (
                                                        <Text style={styles1.pardiffnumber}>+ {parDifferential}</Text>
                                                    )}
                                                    {parDifferential <= 0 && (
                                                        <Text style={styles1.pardiffnumber}>{parDifferential}</Text>
                                                    )}
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={styles1.column}>
                                                <Text style={styles1.columnText}>PPH:</Text>
                                                <Text style={styles1.columnText}>Score:</Text>
                                                <Text style={styles1.columnText}>FIR Avg:</Text>
                                                <Text style={styles1.columnText}>GIR Avg:</Text>
                                            </View>
                                            <View style={styles1.column1}>
                                                <Text style={styles1.columnscore}>{avgPutts}</Text>
                                                <Text style={styles1.columnscore}>{score}</Text>
                                                <Text style={styles1.columnscore}>{avgFir}/9</Text>
                                                <Text style={styles1.columnscore}>{avgGir}/9</Text>
                                            </View>
                                        </View>
                                    </View>
                                <View style={styles1.holeinfocontainer}>
                                    <View style={styles1.holeinputcontainer}>
                                        <View style={styles1.toprowcontainer}>
                                            <View style={styles1.parinputcontainer}>
                                                <View style={styles1.parintputheader}>
                                                        <Text style={styles1.puttstext}>Hole Par</Text>
                                                </View>
                                                    <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole2Par}
                                                    />
                                            </View>
                                            <View style={styles1.girfircontainer}>
                                                <View style={styles1.firinputcontainer}>
                                                    <View style={styles1.firintputheader}>
                                                            <Text style={styles1.puttstext}>FIR</Text>
                                                    </View>
                                                    <View style={styles1.fircheckbox}>
                                                        <Pressable onPress={handle2FirPress} style={styles1.fircheckboxstyle}>
                                                            <MaterialIcons
                                                            name={is2FirChecked ? 'check-box' : 'check-box-outline-blank'}
                                                            size={70}
                                                            color="white"
                                                            />
                                                        </Pressable>
                                                        <Text>{is2FirChecked ? 'Checked' : 'Unchecked'}</Text>
                                                    </View>
                                                </View>
                                                <View style={styles1.girinputcontainer}>
                                                    <View style={styles1.girintputheader}>
                                                            <Text style={styles1.puttstext}>GIR</Text>
                                                    </View>
                                                    <View style={styles1.fircheckbox}>
                                                        <Pressable onPress={handle2GirPress} style={styles1.fircheckboxstyle}>
                                                            <MaterialIcons
                                                            name={is2GirChecked ? 'check-box' : 'check-box-outline-blank'}
                                                            size={70}
                                                            color="white"
                                                            />
                                                        </Pressable>
                                                        <Text>{is2GirChecked ? 'Checked' : 'Unchecked'}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={styles1.holenumbercontainer}>
                                                <Text style={styles1.holenumbertext}>Hole #2</Text>
                                            </View>
                                        </View>
                                        <View style={styles1.bottomrowcontainer}>
                                            <View style={styles1.puttscontainer}>
                                                <View style={styles1.puttsheader}>
                                                    <Text style={styles1.puttstext}>Putts</Text>
                                                </View>
                                                <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole2Putts}
                                                    />
                                            </View>
                                            <View style={styles1.scorecontainter}>
                                                <View style={styles1.scoreheader}>
                                                    <Text style={styles1.puttstext}>Score</Text>
                                                </View>
                                                <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole2Score}
                                                    />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles1.holescrollcontainer}>
                                        <View style={styles1.horizontalContainer}>
                                            <Pressable
                                            onPress={() => hole2ScrollDownToIndex(index + 1)}
                                            style={styles1.holescrollbuttonright}
                                            >
                                                <MaterialIcons name="keyboard-arrow-down" size={40} color="black" />
                                            </Pressable>
                                        </View>
                                    </View>
                                </View>
                            </>
                            // this is the end of the entire hole2screen
                            )}
                            {item.key.startsWith('Hole Three') && (
                            // this is the start of hole 3 stuff, the entire hole3 input screen
                            <>
                            <View style={styles1.profileSafeAreaExtend}>
                                        <View style={styles1.headercontainer}>
                                            <Pressable 
                                            style={styles1.cancelroundbutton}
                                            onPress={handleCancel}
                                            >
                                                <Text style={styles1.cancelroundbuttontext}>Cancel Round</Text>
                                            </Pressable>
                                            <View style={styles1.statsheadercontainer}>
                                                <Text style={styles1.statsheader}>Stats</Text>
                                            </View>
                                        </View>
                                        <View style={styles1.statscontainer}>
                                            <View style={styles1.column0}>
                                                <View style={styles1.pardiffcontainer}>
                                                    <View style={styles1.pardiffheader}>
                                                        <Text style={styles1.pardifftext}>Par Differential</Text>
                                                    </View>
                                                    <View style={styles1.pardiffnumbercontainer}>
                                                    {parDifferential > 0 && (
                                                        <Text style={styles1.pardiffnumber}>+ {parDifferential}</Text>
                                                    )}
                                                    {parDifferential <= 0 && (
                                                        <Text style={styles1.pardiffnumber}>{parDifferential}</Text>
                                                    )}
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={styles1.column}>
                                                <Text style={styles1.columnText}>PPH:</Text>
                                                <Text style={styles1.columnText}>Score:</Text>
                                                <Text style={styles1.columnText}>FIR:</Text>
                                                <Text style={styles1.columnText}>GIR:</Text>
                                            </View>
                                            <View style={styles1.column1}>
                                                <Text style={styles1.columnscore}>{avgPutts}</Text>
                                                <Text style={styles1.columnscore}>{score}</Text>
                                                <Text style={styles1.columnscore}>{avgFir}/9</Text>
                                                <Text style={styles1.columnscore}>{avgGir}/9</Text>
                                            </View>
                                        </View>
                                    </View>
                                <View style={styles1.holeinfocontainer}>
                                    <View style={styles1.holeinputcontainer}>
                                        <View style={styles1.toprowcontainer}>
                                            <View style={styles1.parinputcontainer}>
                                                <View style={styles1.parintputheader}>
                                                        <Text style={styles1.puttstext}>Hole Par</Text>
                                                </View>
                                                    <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole3Par}
                                                    />
                                            </View>
                                            <View style={styles1.girfircontainer}>
                                                <View style={styles1.firinputcontainer}>
                                                    <View style={styles1.firintputheader}>
                                                            <Text style={styles1.puttstext}>FIR</Text>
                                                    </View>
                                                    <View style={styles1.fircheckbox}>
                                                        <Pressable onPress={handle3FirPress} style={styles1.fircheckboxstyle}>
                                                            <MaterialIcons
                                                            name={is3FirChecked ? 'check-box' : 'check-box-outline-blank'}
                                                            size={70}
                                                            color="white"
                                                            />
                                                        </Pressable>
                                                        <Text>{is3FirChecked ? 'Checked' : 'Unchecked'}</Text>
                                                    </View>
                                                </View>
                                                <View style={styles1.girinputcontainer}>
                                                    <View style={styles1.girintputheader}>
                                                            <Text style={styles1.puttstext}>GIR</Text>
                                                    </View>
                                                    <View style={styles1.fircheckbox}>
                                                        <Pressable onPress={handle3GirPress} style={styles1.fircheckboxstyle}>
                                                            <MaterialIcons
                                                            name={is3GirChecked ? 'check-box' : 'check-box-outline-blank'}
                                                            size={70}
                                                            color="white"
                                                            />
                                                        </Pressable>
                                                        <Text>{is3GirChecked ? 'Checked' : 'Unchecked'}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={styles1.holenumbercontainer}>
                                                <Text style={styles1.holenumbertext}>Hole #3</Text>
                                            </View>
                                        </View>
                                        <View style={styles1.bottomrowcontainer}>
                                            <View style={styles1.puttscontainer}>
                                                <View style={styles1.puttsheader}>
                                                    <Text style={styles1.puttstext}>Putts</Text>
                                                </View>
                                                <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole3Putts}
                                                    />
                                            </View>
                                            <View style={styles1.scorecontainter}>
                                                <View style={styles1.scoreheader}>
                                                    <Text style={styles1.puttstext}>Score</Text>
                                                </View>
                                                <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole3Score}
                                                    />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles1.holescrollcontainer}>
                                        <View style={styles1.horizontalContainer}>
                                            <Pressable
                                            onPress={() => hole3ScrollDownToIndex(index + 1)}
                                            style={styles1.holescrollbuttonright}
                                            >
                                                <MaterialIcons name="keyboard-arrow-down" size={40} color="black" />
                                            </Pressable>
                                        </View>
                                    </View>
                                </View>
                            </>
                            // this is the end of the entire hole3screen
                            )}
                            {item.key.startsWith('Hole Four') && (
                            // this is the start of hole 4 stuff, the entire hole4 input screen
                            <>
                            <View style={styles1.profileSafeAreaExtend}>
                                        <View style={styles1.headercontainer}>
                                            <Pressable 
                                            style={styles1.cancelroundbutton}
                                            onPress={handleCancel}
                                            >
                                                <Text style={styles1.cancelroundbuttontext}>Cancel Round</Text>
                                            </Pressable>
                                            <View style={styles1.statsheadercontainer}>
                                                <Text style={styles1.statsheader}>Stats</Text>
                                            </View>
                                        </View>
                                        <View style={styles1.statscontainer}>
                                            <View style={styles1.column0}>
                                                <View style={styles1.pardiffcontainer}>
                                                    <View style={styles1.pardiffheader}>
                                                        <Text style={styles1.pardifftext}>Par Differential</Text>
                                                    </View>
                                                    <View style={styles1.pardiffnumbercontainer}>
                                                    {parDifferential > 0 && (
                                                        <Text style={styles1.pardiffnumber}>+ {parDifferential}</Text>
                                                    )}
                                                    {parDifferential <= 0 && (
                                                        <Text style={styles1.pardiffnumber}>{parDifferential}</Text>
                                                    )}
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={styles1.column}>
                                                <Text style={styles1.columnText}>PPH:</Text>
                                                <Text style={styles1.columnText}>Score:</Text>
                                                <Text style={styles1.columnText}>FIR:</Text>
                                                <Text style={styles1.columnText}>GIR:</Text>
                                            </View>
                                            <View style={styles1.column1}>
                                                <Text style={styles1.columnscore}>{avgPutts}</Text>
                                                <Text style={styles1.columnscore}>{score}</Text>
                                                <Text style={styles1.columnscore}>{avgFir}/9</Text>
                                                <Text style={styles1.columnscore}>{avgGir}/9</Text>
                                            </View>
                                        </View>
                                    </View>
                                <View style={styles1.holeinfocontainer}>
                                    <View style={styles1.holeinputcontainer}>
                                        <View style={styles1.toprowcontainer}>
                                            <View style={styles1.parinputcontainer}>
                                                <View style={styles1.parintputheader}>
                                                        <Text style={styles1.puttstext}>Hole Par</Text>
                                                </View>
                                                    <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole4Par}
                                                    />
                                            </View>
                                            <View style={styles1.girfircontainer}>
                                                <View style={styles1.firinputcontainer}>
                                                    <View style={styles1.firintputheader}>
                                                            <Text style={styles1.puttstext}>FIR</Text>
                                                    </View>
                                                    <View style={styles1.fircheckbox}>
                                                        <Pressable onPress={handle4FirPress} style={styles1.fircheckboxstyle}>
                                                            <MaterialIcons
                                                            name={is4FirChecked ? 'check-box' : 'check-box-outline-blank'}
                                                            size={70}
                                                            color="white"
                                                            />
                                                        </Pressable>
                                                        <Text>{is4FirChecked ? 'Checked' : 'Unchecked'}</Text>
                                                    </View>
                                                </View>
                                                <View style={styles1.girinputcontainer}>
                                                    <View style={styles1.girintputheader}>
                                                            <Text style={styles1.puttstext}>GIR</Text>
                                                    </View>
                                                    <View style={styles1.fircheckbox}>
                                                        <Pressable onPress={handle4GirPress} style={styles1.fircheckboxstyle}>
                                                            <MaterialIcons
                                                            name={is4GirChecked ? 'check-box' : 'check-box-outline-blank'}
                                                            size={70}
                                                            color="white"
                                                            />
                                                        </Pressable>
                                                        <Text>{is4GirChecked ? 'Checked' : 'Unchecked'}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={styles1.holenumbercontainer}>
                                                <Text style={styles1.holenumbertext}>Hole #4</Text>
                                            </View>
                                        </View>
                                        <View style={styles1.bottomrowcontainer}>
                                            <View style={styles1.puttscontainer}>
                                                <View style={styles1.puttsheader}>
                                                    <Text style={styles1.puttstext}>Putts</Text>
                                                </View>
                                                <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole4Putts}
                                                    />
                                            </View>
                                            <View style={styles1.scorecontainter}>
                                                <View style={styles1.scoreheader}>
                                                    <Text style={styles1.puttstext}>Score</Text>
                                                </View>
                                                <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole4Score}
                                                    />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles1.holescrollcontainer}>
                                        <View style={styles1.horizontalContainer}>
                                            <Pressable
                                            onPress={() => hole4ScrollDownToIndex(index + 1)}
                                            style={styles1.holescrollbuttonright}
                                            >
                                                <MaterialIcons name="keyboard-arrow-down" size={40} color="black" />
                                            </Pressable>
                                        </View>
                                    </View>
                                </View>
                            </>
                            // this is the end of the entire hole5screen
                            )}
                            {item.key.startsWith('Hole Five') && (
                            // this is the start of hole 5 stuff, the entire hole5 input screen
                            <>
                            <View style={styles1.profileSafeAreaExtend}>
                                        <View style={styles1.headercontainer}>
                                            <Pressable 
                                            style={styles1.cancelroundbutton}
                                            onPress={handleCancel}
                                            >
                                                <Text style={styles1.cancelroundbuttontext}>Cancel Round</Text>
                                            </Pressable>
                                            <View style={styles1.statsheadercontainer}>
                                                <Text style={styles1.statsheader}>Stats</Text>
                                            </View>
                                        </View>
                                        <View style={styles1.statscontainer}>
                                            <View style={styles1.column0}>
                                                <View style={styles1.pardiffcontainer}>
                                                    <View style={styles1.pardiffheader}>
                                                        <Text style={styles1.pardifftext}>Par Differential</Text>
                                                    </View>
                                                    <View style={styles1.pardiffnumbercontainer}>
                                                    {parDifferential > 0 && (
                                                        <Text style={styles1.pardiffnumber}>+ {parDifferential}</Text>
                                                    )}
                                                    {parDifferential <= 0 && (
                                                        <Text style={styles1.pardiffnumber}>{parDifferential}</Text>
                                                    )}
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={styles1.column}>
                                                <Text style={styles1.columnText}>PPH:</Text>
                                                <Text style={styles1.columnText}>Score:</Text>
                                                <Text style={styles1.columnText}>FIR:</Text>
                                                <Text style={styles1.columnText}>GIR:</Text>
                                            </View>
                                            <View style={styles1.column1}>
                                                <Text style={styles1.columnscore}>{avgPutts}</Text>
                                                <Text style={styles1.columnscore}>{score}</Text>
                                                <Text style={styles1.columnscore}>{avgFir}/9</Text>
                                                <Text style={styles1.columnscore}>{avgGir}/9</Text>
                                            </View>
                                        </View>
                                    </View>
                                <View style={styles1.holeinfocontainer}>
                                    <View style={styles1.holeinputcontainer}>
                                        <View style={styles1.toprowcontainer}>
                                            <View style={styles1.parinputcontainer}>
                                                <View style={styles1.parintputheader}>
                                                        <Text style={styles1.puttstext}>Hole Par</Text>
                                                </View>
                                                    <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole5Par}
                                                    />
                                            </View>
                                            <View style={styles1.girfircontainer}>
                                                <View style={styles1.firinputcontainer}>
                                                    <View style={styles1.firintputheader}>
                                                            <Text style={styles1.puttstext}>FIR</Text>
                                                    </View>
                                                    <View style={styles1.fircheckbox}>
                                                        <Pressable onPress={handle5FirPress} style={styles1.fircheckboxstyle}>
                                                            <MaterialIcons
                                                            name={is5FirChecked ? 'check-box' : 'check-box-outline-blank'}
                                                            size={70}
                                                            color="white"
                                                            />
                                                        </Pressable>
                                                        <Text>{is5FirChecked ? 'Checked' : 'Unchecked'}</Text>
                                                    </View>
                                                </View>
                                                <View style={styles1.girinputcontainer}>
                                                    <View style={styles1.girintputheader}>
                                                            <Text style={styles1.puttstext}>GIR</Text>
                                                    </View>
                                                    <View style={styles1.fircheckbox}>
                                                        <Pressable onPress={handle5GirPress} style={styles1.fircheckboxstyle}>
                                                            <MaterialIcons
                                                            name={is5GirChecked ? 'check-box' : 'check-box-outline-blank'}
                                                            size={70}
                                                            color="white"
                                                            />
                                                        </Pressable>
                                                        <Text>{is5GirChecked ? 'Checked' : 'Unchecked'}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={styles1.holenumbercontainer}>
                                                <Text style={styles1.holenumbertext}>Hole #5</Text>
                                            </View>
                                        </View>
                                        <View style={styles1.bottomrowcontainer}>
                                            <View style={styles1.puttscontainer}>
                                                <View style={styles1.puttsheader}>
                                                    <Text style={styles1.puttstext}>Putts</Text>
                                                </View>
                                                <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole5Putts}
                                                    />
                                            </View>
                                            <View style={styles1.scorecontainter}>
                                                <View style={styles1.scoreheader}>
                                                    <Text style={styles1.puttstext}>Score</Text>
                                                </View>
                                                <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole5Score}
                                                    />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles1.holescrollcontainer}>
                                        <View style={styles1.horizontalContainer}>
                                            <Pressable
                                            onPress={() => hole5ScrollDownToIndex(index + 1)}
                                            style={styles1.holescrollbuttonright}
                                            >
                                                <MaterialIcons name="keyboard-arrow-down" size={40} color="black" />
                                            </Pressable>
                                        </View>
                                    </View>
                                </View>
                            </>
                            // this is the end of the entire hole5screen
                            )}
                            {item.key.startsWith('Hole Six') && (
                            // this is the start of hole 6 stuff, the entire hole5 input screen
                            <>
                            <View style={styles1.profileSafeAreaExtend}>
                                        <View style={styles1.headercontainer}>
                                            <Pressable 
                                            style={styles1.cancelroundbutton}
                                            onPress={handleCancel}
                                            >
                                                <Text style={styles1.cancelroundbuttontext}>Cancel Round</Text>
                                            </Pressable>
                                            <View style={styles1.statsheadercontainer}>
                                                <Text style={styles1.statsheader}>Stats</Text>
                                            </View>
                                        </View>
                                        <View style={styles1.statscontainer}>
                                            <View style={styles1.column0}>
                                                <View style={styles1.pardiffcontainer}>
                                                    <View style={styles1.pardiffheader}>
                                                        <Text style={styles1.pardifftext}>Par Differential</Text>
                                                    </View>
                                                    <View style={styles1.pardiffnumbercontainer}>
                                                    {parDifferential > 0 && (
                                                        <Text style={styles1.pardiffnumber}>+ {parDifferential}</Text>
                                                    )}
                                                    {parDifferential <= 0 && (
                                                        <Text style={styles1.pardiffnumber}>{parDifferential}</Text>
                                                    )}
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={styles1.column}>
                                                <Text style={styles1.columnText}>PPH:</Text>
                                                <Text style={styles1.columnText}>Score:</Text>
                                                <Text style={styles1.columnText}>FIR:</Text>
                                                <Text style={styles1.columnText}>GIR:</Text>
                                            </View>
                                            <View style={styles1.column1}>
                                                <Text style={styles1.columnscore}>{avgPutts}</Text>
                                                <Text style={styles1.columnscore}>{score}</Text>
                                                <Text style={styles1.columnscore}>{avgFir}/9</Text>
                                                <Text style={styles1.columnscore}>{avgGir}/9</Text>
                                            </View>
                                        </View>
                                    </View>
                                <View style={styles1.holeinfocontainer}>
                                    <View style={styles1.holeinputcontainer}>
                                        <View style={styles1.toprowcontainer}>
                                            <View style={styles1.parinputcontainer}>
                                                <View style={styles1.parintputheader}>
                                                        <Text style={styles1.puttstext}>Hole Par</Text>
                                                </View>
                                                    <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole6Par}
                                                    />
                                            </View>
                                            <View style={styles1.girfircontainer}>
                                                <View style={styles1.firinputcontainer}>
                                                    <View style={styles1.firintputheader}>
                                                            <Text style={styles1.puttstext}>FIR</Text>
                                                    </View>
                                                    <View style={styles1.fircheckbox}>
                                                        <Pressable onPress={handle6FirPress} style={styles1.fircheckboxstyle}>
                                                            <MaterialIcons
                                                            name={is6FirChecked ? 'check-box' : 'check-box-outline-blank'}
                                                            size={70}
                                                            color="white"
                                                            />
                                                        </Pressable>
                                                        <Text>{is6FirChecked ? 'Checked' : 'Unchecked'}</Text>
                                                    </View>
                                                </View>
                                                <View style={styles1.girinputcontainer}>
                                                    <View style={styles1.girintputheader}>
                                                            <Text style={styles1.puttstext}>GIR</Text>
                                                    </View>
                                                    <View style={styles1.fircheckbox}>
                                                        <Pressable onPress={handle6GirPress} style={styles1.fircheckboxstyle}>
                                                            <MaterialIcons
                                                            name={is6GirChecked ? 'check-box' : 'check-box-outline-blank'}
                                                            size={70}
                                                            color="white"
                                                            />
                                                        </Pressable>
                                                        <Text>{is6GirChecked ? 'Checked' : 'Unchecked'}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={styles1.holenumbercontainer}>
                                                <Text style={styles1.holenumbertext}>Hole #6</Text>
                                            </View>
                                        </View>
                                        <View style={styles1.bottomrowcontainer}>
                                            <View style={styles1.puttscontainer}>
                                                <View style={styles1.puttsheader}>
                                                    <Text style={styles1.puttstext}>Putts</Text>
                                                </View>
                                                <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole6Putts}
                                                    />
                                            </View>
                                            <View style={styles1.scorecontainter}>
                                                <View style={styles1.scoreheader}>
                                                    <Text style={styles1.puttstext}>Score</Text>
                                                </View>
                                                <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole6Score}
                                                    />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles1.holescrollcontainer}>
                                        <View style={styles1.horizontalContainer}>
                                            <Pressable
                                            onPress={() => hole6ScrollDownToIndex(index + 1)}
                                            style={styles1.holescrollbuttonright}
                                            >
                                                <MaterialIcons name="keyboard-arrow-down" size={40} color="black" />
                                            </Pressable>
                                        </View>
                                    </View>
                                </View>
                            </>
                            // this is the end of the entire hole6screen
                            )}
                            {item.key.startsWith('Hole Seven') && (
                            // this is the start of hole 7 stuff, the entire hole7 input screen
                            <>
                            <View style={styles1.profileSafeAreaExtend}>
                                        <View style={styles1.headercontainer}>
                                            <Pressable 
                                            style={styles1.cancelroundbutton}
                                            onPress={handleCancel}
                                            >
                                                <Text style={styles1.cancelroundbuttontext}>Cancel Round</Text>
                                            </Pressable>
                                            <View style={styles1.statsheadercontainer}>
                                                <Text style={styles1.statsheader}>Stats</Text>
                                            </View>
                                        </View>
                                        <View style={styles1.statscontainer}>
                                            <View style={styles1.column0}>
                                                <View style={styles1.pardiffcontainer}>
                                                    <View style={styles1.pardiffheader}>
                                                        <Text style={styles1.pardifftext}>Par Differential</Text>
                                                    </View>
                                                    <View style={styles1.pardiffnumbercontainer}>
                                                    {parDifferential > 0 && (
                                                        <Text style={styles1.pardiffnumber}>+ {parDifferential}</Text>
                                                    )}
                                                    {parDifferential <= 0 && (
                                                        <Text style={styles1.pardiffnumber}>{parDifferential}</Text>
                                                    )}
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={styles1.column}>
                                                <Text style={styles1.columnText}>PPH:</Text>
                                                <Text style={styles1.columnText}>Score:</Text>
                                                <Text style={styles1.columnText}>FIR:</Text>
                                                <Text style={styles1.columnText}>GIR:</Text>
                                            </View>
                                            <View style={styles1.column1}>
                                                <Text style={styles1.columnscore}>{avgPutts}</Text>
                                                <Text style={styles1.columnscore}>{score}</Text>
                                                <Text style={styles1.columnscore}>{avgFir}/9</Text>
                                                <Text style={styles1.columnscore}>{avgGir}/9</Text>
                                            </View>
                                        </View>
                                    </View>
                                <View style={styles1.holeinfocontainer}>
                                    <View style={styles1.holeinputcontainer}>
                                        <View style={styles1.toprowcontainer}>
                                            <View style={styles1.parinputcontainer}>
                                                <View style={styles1.parintputheader}>
                                                        <Text style={styles1.puttstext}>Hole Par</Text>
                                                </View>
                                                    <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole7Par}
                                                    />
                                            </View>
                                            <View style={styles1.girfircontainer}>
                                                <View style={styles1.firinputcontainer}>
                                                    <View style={styles1.firintputheader}>
                                                            <Text style={styles1.puttstext}>FIR</Text>
                                                    </View>
                                                    <View style={styles1.fircheckbox}>
                                                        <Pressable onPress={handle7FirPress} style={styles1.fircheckboxstyle}>
                                                            <MaterialIcons
                                                            name={is7FirChecked ? 'check-box' : 'check-box-outline-blank'}
                                                            size={70}
                                                            color="white"
                                                            />
                                                        </Pressable>
                                                        <Text>{is7FirChecked ? 'Checked' : 'Unchecked'}</Text>
                                                    </View>
                                                </View>
                                                <View style={styles1.girinputcontainer}>
                                                    <View style={styles1.girintputheader}>
                                                            <Text style={styles1.puttstext}>GIR</Text>
                                                    </View>
                                                    <View style={styles1.fircheckbox}>
                                                        <Pressable onPress={handle7GirPress} style={styles1.fircheckboxstyle}>
                                                            <MaterialIcons
                                                            name={is7GirChecked ? 'check-box' : 'check-box-outline-blank'}
                                                            size={70}
                                                            color="white"
                                                            />
                                                        </Pressable>
                                                        <Text>{is7GirChecked ? 'Checked' : 'Unchecked'}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={styles1.holenumbercontainer}>
                                                <Text style={styles1.holenumbertext}>Hole #7</Text>
                                            </View>
                                        </View>
                                        <View style={styles1.bottomrowcontainer}>
                                            <View style={styles1.puttscontainer}>
                                                <View style={styles1.puttsheader}>
                                                    <Text style={styles1.puttstext}>Putts</Text>
                                                </View>
                                                <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole7Putts}
                                                    />
                                            </View>
                                            <View style={styles1.scorecontainter}>
                                                <View style={styles1.scoreheader}>
                                                    <Text style={styles1.puttstext}>Score</Text>
                                                </View>
                                                <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole7Score}
                                                    />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles1.holescrollcontainer}>
                                        <View style={styles1.horizontalContainer}>
                                            <Pressable
                                            onPress={() => hole7ScrollDownToIndex(index + 1)}
                                            style={styles1.holescrollbuttonright}
                                            >
                                                <MaterialIcons name="keyboard-arrow-down" size={40} color="black" />
                                            </Pressable>
                                        </View>
                                    </View>
                                </View>
                            </>
                            // this is the end of the entire hole7screen
                            )}
                            {item.key.startsWith('Hole Eight') && (
                            // this is the start of hole 8 stuff, the entire hole8 input screen
                            <>
                            <View style={styles1.profileSafeAreaExtend}>
                                        <View style={styles1.headercontainer}>
                                            <Pressable 
                                            style={styles1.cancelroundbutton}
                                            onPress={handleCancel}
                                            >
                                                <Text style={styles1.cancelroundbuttontext}>Cancel Round</Text>
                                            </Pressable>
                                            <View style={styles1.statsheadercontainer}>
                                                <Text style={styles1.statsheader}>Stats</Text>
                                            </View>
                                        </View>
                                        <View style={styles1.statscontainer}>
                                            <View style={styles1.column0}>
                                                <View style={styles1.pardiffcontainer}>
                                                    <View style={styles1.pardiffheader}>
                                                        <Text style={styles1.pardifftext}>Par Differential</Text>
                                                    </View>
                                                    <View style={styles1.pardiffnumbercontainer}>
                                                    {parDifferential > 0 && (
                                                        <Text style={styles1.pardiffnumber}>+ {parDifferential}</Text>
                                                    )}
                                                    {parDifferential <= 0 && (
                                                        <Text style={styles1.pardiffnumber}>{parDifferential}</Text>
                                                    )}
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={styles1.column}>
                                                <Text style={styles1.columnText}>PPH:</Text>
                                                <Text style={styles1.columnText}>Score:</Text>
                                                <Text style={styles1.columnText}>FIR:</Text>
                                                <Text style={styles1.columnText}>GIR:</Text>
                                            </View>
                                            <View style={styles1.column1}>
                                                <Text style={styles1.columnscore}>{avgPutts}</Text>
                                                <Text style={styles1.columnscore}>{score}</Text>
                                                <Text style={styles1.columnscore}>{avgFir}/9</Text>
                                                <Text style={styles1.columnscore}>{avgGir}/9</Text>
                                            </View>
                                        </View>
                                    </View>
                                <View style={styles1.holeinfocontainer}>
                                    <View style={styles1.holeinputcontainer}>
                                        <View style={styles1.toprowcontainer}>
                                            <View style={styles1.parinputcontainer}>
                                                <View style={styles1.parintputheader}>
                                                        <Text style={styles1.puttstext}>Hole Par</Text>
                                                </View>
                                                    <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole8Par}
                                                    />
                                            </View>
                                            <View style={styles1.girfircontainer}>
                                                <View style={styles1.firinputcontainer}>
                                                    <View style={styles1.firintputheader}>
                                                            <Text style={styles1.puttstext}>FIR</Text>
                                                    </View>
                                                    <View style={styles1.fircheckbox}>
                                                        <Pressable onPress={handle8FirPress} style={styles1.fircheckboxstyle}>
                                                            <MaterialIcons
                                                            name={is8FirChecked ? 'check-box' : 'check-box-outline-blank'}
                                                            size={70}
                                                            color="white"
                                                            />
                                                        </Pressable>
                                                        <Text>{is8FirChecked ? 'Checked' : 'Unchecked'}</Text>
                                                    </View>
                                                </View>
                                                <View style={styles1.girinputcontainer}>
                                                    <View style={styles1.girintputheader}>
                                                            <Text style={styles1.puttstext}>GIR</Text>
                                                    </View>
                                                    <View style={styles1.fircheckbox}>
                                                        <Pressable onPress={handle8GirPress} style={styles1.fircheckboxstyle}>
                                                            <MaterialIcons
                                                            name={is8GirChecked ? 'check-box' : 'check-box-outline-blank'}
                                                            size={70}
                                                            color="white"
                                                            />
                                                        </Pressable>
                                                        <Text>{is8GirChecked ? 'Checked' : 'Unchecked'}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={styles1.holenumbercontainer}>
                                                <Text style={styles1.holenumbertext}>Hole #8</Text>
                                            </View>
                                        </View>
                                        <View style={styles1.bottomrowcontainer}>
                                            <View style={styles1.puttscontainer}>
                                                <View style={styles1.puttsheader}>
                                                    <Text style={styles1.puttstext}>Putts</Text>
                                                </View>
                                                <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole8Putts}
                                                    />
                                            </View>
                                            <View style={styles1.scorecontainter}>
                                                <View style={styles1.scoreheader}>
                                                    <Text style={styles1.puttstext}>Score</Text>
                                                </View>
                                                <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole8Score}
                                                    />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles1.holescrollcontainer}>
                                        <View style={styles1.horizontalContainer}>
                                            <Pressable
                                            onPress={() => hole8ScrollDownToIndex(index + 1)}
                                            style={styles1.holescrollbuttonright}
                                            >
                                                <MaterialIcons name="keyboard-arrow-down" size={40} color="black" />
                                            </Pressable>
                                        </View>
                                    </View>
                                </View>
                            </>
                            // this is the end of the entire hole8screen
                            )}
                            {item.key.startsWith('Hole Nine') && (
                            // this is the start of hole 9 stuff, the entire hole5 input screen
                            <>
                            <View style={styles1.profileSafeAreaExtend}>
                                        <View style={styles1.headercontainer}>
                                            <Pressable 
                                            style={styles1.cancelroundbutton}
                                            onPress={handleCancel}
                                            >
                                                <Text style={styles1.cancelroundbuttontext}>Cancel Round</Text>
                                            </Pressable>
                                            <View style={styles1.statsheadercontainer}>
                                                <Text style={styles1.statsheader}>Stats</Text>
                                            </View>
                                        </View>
                                        <View style={styles1.statscontainer}>
                                            <View style={styles1.column0}>
                                                <View style={styles1.pardiffcontainer}>
                                                    <View style={styles1.pardiffheader}>
                                                        <Text style={styles1.pardifftext}>Par Differential</Text>
                                                    </View>
                                                    <View style={styles1.pardiffnumbercontainer}>
                                                    {parDifferential > 0 && (
                                                        <Text style={styles1.pardiffnumber}>+ {parDifferential}</Text>
                                                    )}
                                                    {parDifferential <= 0 && (
                                                        <Text style={styles1.pardiffnumber}>{parDifferential}</Text>
                                                    )}
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={styles1.column}>
                                                <Text style={styles1.columnText}>PPH:</Text>
                                                <Text style={styles1.columnText}>Score:</Text>
                                                <Text style={styles1.columnText}>FIR:</Text>
                                                <Text style={styles1.columnText}>GIR:</Text>
                                            </View>
                                            <View style={styles1.column1}>
                                                <Text style={styles1.columnscore}>{avgPutts}</Text>
                                                <Text style={styles1.columnscore}>{score}</Text>
                                                <Text style={styles1.columnscore}>{avgFir}/9</Text>
                                                <Text style={styles1.columnscore}>{avgGir}/9</Text>
                                            </View>
                                        </View>
                                    </View>
                                <View style={styles1.holeinfocontainer}>
                                    <View style={styles1.holeinputcontainer}>
                                        <View style={styles1.toprowcontainer}>
                                            <View style={styles1.parinputcontainer}>
                                                <View style={styles1.parintputheader}>
                                                        <Text style={styles1.puttstext}>Hole Par</Text>
                                                </View>
                                                    <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole9Par}
                                                    />
                                            </View>
                                            <View style={styles1.girfircontainer}>
                                                <View style={styles1.firinputcontainer}>
                                                    <View style={styles1.firintputheader}>
                                                            <Text style={styles1.puttstext}>FIR</Text>
                                                    </View>
                                                    <View style={styles1.fircheckbox}>
                                                        <Pressable onPress={handle9FirPress} style={styles1.fircheckboxstyle}>
                                                            <MaterialIcons
                                                            name={is9FirChecked ? 'check-box' : 'check-box-outline-blank'}
                                                            size={70}
                                                            color="white"
                                                            />
                                                        </Pressable>
                                                        <Text>{is9FirChecked ? 'Checked' : 'Unchecked'}</Text>
                                                    </View>
                                                </View>
                                                <View style={styles1.girinputcontainer}>
                                                    <View style={styles1.girintputheader}>
                                                            <Text style={styles1.puttstext}>GIR</Text>
                                                    </View>
                                                    <View style={styles1.fircheckbox}>
                                                        <Pressable onPress={handle9GirPress} style={styles1.fircheckboxstyle}>
                                                            <MaterialIcons
                                                            name={is9GirChecked ? 'check-box' : 'check-box-outline-blank'}
                                                            size={70}
                                                            color="white"
                                                            />
                                                        </Pressable>
                                                        <Text>{is9GirChecked ? 'Checked' : 'Unchecked'}</Text>
                                                    </View>
                                                </View>
                                            </View>
                                            <View style={styles1.holenumbercontainer}>
                                                <Text style={styles1.holenumbertext}>Hole #9</Text>
                                            </View>
                                        </View>
                                        <View style={styles1.bottomrowcontainer}>
                                            <View style={styles1.puttscontainer}>
                                                <View style={styles1.puttsheader}>
                                                    <Text style={styles1.puttstext}>Putts</Text>
                                                </View>
                                                <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole9Putts}
                                                    />
                                            </View>
                                            <View style={styles1.scorecontainter}>
                                                <View style={styles1.scoreheader}>
                                                    <Text style={styles1.puttstext}>Score</Text>
                                                </View>
                                                <TextInput
                                                    style={styles1.parinput}
                                                    keyboardType="numeric"
                                                    placeholder="0" 
                                                    onChangeText={handleHole9Score}
                                                    />
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles1.holescrollcontainer}>
                                        <View style={styles1.horizontalContainer}>
                                            <Pressable
                                            onPress={() => hole9ScrollDownToIndex(index + 1)}
                                            style={styles1.holescrollbuttonright}
                                            >
                                                <MaterialIcons name="add" size={40} color="black" />
                                            </Pressable>
                                        </View>
                                    </View>
                                </View>
                            </>
                            // this is the end of the entire hole9screen
                            )}
                            {item.key.startsWith('Round Stats') && (
                            // this is the start of final stats stuff
                            <>
                           <View style={styles1.endingstatspage}>
                                <View style={styles1.finalstatscard}>
                                    <View style={styles1.cardheadercontainer}>
                                        <Text style={styles1.statsheadertext}>Round Stats</Text>
                                    </View>
                                    <View style={styles1.finalstatscontainer}>
                                        <Text style={styles1.finalstats}>Score: {score} </Text>
                                        {parDifferential > 0 && (
                                                        <Text style={styles1.finalstats}>Par Differential: + {parDifferential}</Text>
                                                    )}
                                                    {parDifferential <= 0 && (
                                                        <Text style={styles1.finalstats}>Par Differential: {parDifferential}</Text>
                                                    )}
                                        <Text style={styles1.finalstats}>Avg. Fairways in Regulation: {avgFir/18*100}%</Text>
                                        <Text style={styles1.finalstats}>Avg. Greens in Regulation: {avgGir/18*100}%</Text>
                                        <Text style={styles1.finalstats}>Putts Per Hole: {avgPutts}</Text>
                                        <Text style={styles1.finalstats}>Total Putts: {totalPutts}</Text>
                                        <Pressable 
                                            style={styles1.cancelroundbutton1}
                                            onPress={handleCancel}
                                            >
                                            <Text style={styles1.canceltext}>Cancel Scores?</Text>
                                        </Pressable>
                                        <Pressable 
                                            style={styles1.submitroundbutton1}
                                            onPress={handleSubmit}
                                            >
                                            <Text style={styles1.submittext}>Submit</Text>
                                        </Pressable>
                                    </View>
                                </View>
                           </View>
                            </>
                            // this is the end of the entire Final Stats
                            )}
                            </View>
                        </>
                        )}
                        keyExtractor={(item) => item.key}
                        pagingEnabled={true}
                        showsVerticalScrollIndicator={false}
                        scrollEnabled={false}
                    />
                </>
            )
        }

const styles1 = StyleSheet.create({
    submittext: {
        color: 'white',
        fontWeight: '900',
        fontSize: 30,
    },
    canceltext: {
        color: 'white',
        fontWeight: '600',
    },
    submitroundbutton1:{
        backgroundColor: '#333c31',
        height: '10%',
        width: '60%',
        borderWidth: 3,
        borderColor: 'white',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    cancelroundbutton1:{
        backgroundColor: 'red',
        height: '7%',
        width: '40%',
        borderWidth: 3,
        borderColor: 'white',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    finalstats:{
        color: '#333c31',
        fontWeight: '900',
        fontSize: 15,
    },
    finalstatscontainer: {
        zIndex: -1,
        flex: 1,
        backgroundColor: '#a49752',
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30,
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
    statsheadertext: {
        textTransform: 'uppercase',
        fontSize: 40,
        fontWeight: '900',
        color: '#333c31'
    },
    cardheadercontainer: {
        height: '15%',
        backgroundColor: 'white',
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
        borderBottomColor: '#e08c3a',
        borderBottomWidth: 8,
        shadowColor: '#000',
        shadowOffset: { width: 5, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    finalstatscard: {
        height: '90%',
        width: '90%',
        backgroundColor: '#a49752',
        borderRadius: 30,
    },
    endingstatspage: {
        flex: .82,
        justifyContent: 'center',
        alignItems: 'center',
    },
    fircheckboxstyle:{
        height: '90%',
    },
    fircheckbox:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    holenumbertext: {
        color: 'white',
        fontSize: 20,
        marginLeft: -20,
        transform: [{ rotate: '90deg' }],
        textTransform: 'uppercase',
        fontWeight: '900',
    },
    holenumbercontainer:{
        justifyContent: 'center',
        backgroundColor: '#333c31',
        borderBottomWidth: 2,
        borderColor: '#a49752',
    },
    puttstext:{
        color: 'white',
        fontSize: 30,
        fontWeight: '900',
        textTransform: 'uppercase',
    },
    parinput:{
        color: 'white',
        fontSize: 200,
        paddingLeft: 20,
        fontWeight: '900',
        height: '80%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    scoreheader:{
        borderBottomWidth: 2,
        borderColor: '#a49752',
        width: '100%',
        backgroundColor: '#333c31',
        height: '20%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    scorecontainter:{
        borderBottomWidth: 4,
        borderLeftWidth: 2,
        borderTopWidth: 2,
        borderColor: '#a49752',
        width: '50%',
        backgroundColor: 'black',
    },
    puttsheader:{
        borderBottomWidth: 2,
        borderColor: '#a49752',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        backgroundColor: '#333c31',
        height: '20%'
    },
    puttscontainer:{
        borderBottomWidth: 4,
        borderTopWidth: 2,
        borderRightWidth: 2,
        borderColor: '#a49752',
        width: '50%',
        backgroundColor: 'black',
    },
    girfircontainer:{
        backgroundColor: 'pink',
        width: '40%',
        alignItems: 'center',
        borderRightWidth: 4,
        borderBottomWidth: 2,
        borderLeftWidth: 2,
        borderColor: '#a49752',
    },
    girintputheader:{
        borderBottomWidth: 2,
        borderColor: '#a49752',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        backgroundColor: '#333c31',
        height: '40%'
    },
    girinputcontainer: {
        height: '50%',
        width: '100%',
        backgroundColor: 'black'
    },
    firintputheader:{
        borderBottomWidth: 2,
        borderColor: '#a49752',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        backgroundColor: '#333c31',
        height: '42%'
    },
    firinputcontainer: {
        borderBottomWidth: 4,
        borderColor: '#a49752',
        height: '50%',
        width: '100%',
        backgroundColor: 'black'
    },
    parintputheader:{
        borderBottomWidth: 2,
        borderColor: '#a49752',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        backgroundColor: '#333c31',
        height: '20%'
    },
    parinputcontainer:{
        borderRightWidth: 2,
        borderBottomWidth: 2,
        borderColor: '#a49752',
        width: '50%',
        backgroundColor: 'black',
    },
    toprowcontainer:{
        height: '50%',
        flexDirection: 'row',
    },
    bottomrowcontainer:{
        height: '50%',
        backgroundColor: 'grey',
        flexDirection: 'row',
    },
    horizontalContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
      },
    holeinputcontainer: {
        height: '67%',
    },
    holeinfocontainer:{
        flex: 1,
    },
    testfont:{
        color: 'white',
    },
    fullScreenItem: {
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        flex: 1,
        backgroundColor: 'black',
      },
    holescrollbuttonright: {
        height: '90%',
        width: '50%',
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        borderColor: 'black',
        borderWidth: 2,
    },
    holescrollbuttonleft: {
        height: '95%',
        width: '50%',
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        borderTopRightRadius: 25,
        borderBottomRightRadius: 25,
        borderColor: 'black',
        borderWidth: 2,
    },
    scrollbutton: {
        height: '100%',
        width: '100%',
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
    },
    scrolltext: {
        fontSize: 30,
        fontWeight: '700',
        textTransform: 'uppercase',
    },
    holescrollcontainer: {
        height: '10%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    scrollcontainer: {
        padding: '2%',
        height: '10%',
        width: '30%',
        justifyContent: 'center',
        alignItems: 'center',
    },
    profileSafeAreaExtend: {
        height: '21%',
        backgroundColor: '#333c31',
        borderBottomColor: '#a49752',
        borderBottomWidth: 5,
    },
    courseinfocontainer: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: 'black',
    },
    regtextInput: {
        height: '12%',
        borderColor: 'white',
        backgroundColor: '#333c31',
        borderWidth: 5,
        width: '90%',
        fontSize: 40,
        color: 'white',
        borderRadius: 50,
        paddingLeft: '7%',
        margin: 10,
    },
    headercontainer: {
        height: "18%",
        width: '100%',
        flexDirection: 'row',
    },
    cancelroundbutton: {
        height: '100%',
        width: '35%',
        backgroundColor: '#a49752',
        borderTopRightRadius: 20,
        borderBottomRightRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'red',
        borderTopWidth: 3,
        borderRightWidth: 4,
        borderBottomWidth: 4,
        shadowColor: '#000',
        shadowOffset: { width: 5, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
    },
    cancelroundbuttontext: {
        fontSize: 15,
        fontWeight: '900',
        textTransform: 'uppercase'
    },
    cancelpromptcontainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'red',
        margin: 'auto'
    },
    cancelconfirmbutton: {
        height: '20%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
        width: '50%',
        marginTop: 50,
        marginBottom: 50,
        borderRadius: 50,
    },
    cancelconfirmtext: {
        color: 'red',
        fontSize: 90,
        textTransform: 'uppercase',
        fontWeight: '900',
    },
    cancelheader: {
        fontSize: 40,
        marginBottom: 10,
        fontWeight: '700',
        textAlign: 'center',
    },
    canceldetail: {
        fontSize: 18,
        fontWeight: '600',
    },
    statscontainer: {
        marginTop: 10,
        marginBottom: 5,
        flex: 1,
        flexDirection: 'row',
      },
      column0: {
        flex: .5,
        justifyContent: 'center',
        alignItems: 'flex-start',
      },
      column: {
        flex: .3,
        justifyContent: 'center',
        alignItems: 'flex-end',
        marginBottom: 5,
      },
      column1: {
        flex: .2,
        justifyContent: 'center',
        alignItems: 'flex-start',
        marginBottom: 5,
      },
      columnText: {
        fontSize: 20,
        fontWeight: '400',
        color: '#a49752',
        margin: 5,
        shadowColor: '#000',
        shadowOffset: { width: 5, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
      },
      columnscore: {
        fontSize: 20,
        fontWeight: '900',
        color: '#a49752',
        margin: 5,
        shadowColor: '#000',
        shadowOffset: { width: 5, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
      },
      statsheadercontainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%'
      },
      statsheader: {
        fontSize: 30,
        fontWeight: '900',
        textTransform: 'uppercase'
      },
      pardiffcontainer: {
        backgroundColor: '#a49752',
        height: '90%',
        width: '70%',
        borderRadius: 30,
        marginLeft: 50,
        marginBottom: 5,
        shadowColor: '#000',
        shadowOffset: { width: 5, height: 3 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        alignItems: 'center',
      },
      pardiffheader: {
        backgroundColor: '#d7b56b',
        height: '25%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.3,
        shadowRadius: 3,
      },
      pardifftext: {
        fontSize: 15,
        fontWeight: '700',
      },
      pardiffnumbercontainer: {
        height: '70%',
        width: '90%',
        alignItems: 'center',
      },
      pardiffnumber: {
        fontSize: 70,
        fontWeight: '900',
      }
  });

export default NineHoleScorecard
