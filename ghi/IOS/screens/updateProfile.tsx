import { View, TextInput, Text, Pressable, Platform, ActivityIndicator, Image, Modal, Button } from 'react-native'
import React, { useEffect, useState } from 'react'
import * as ImagePicker from 'expo-image-picker';
import  useToken from '../context/AuthContext'
import styles from './styles'

const UpdateProfile = ({ navigation }) => {
    const REACT_APP_API_HOST = 'http://localhost:8000'
    const [loading, setLoading] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");
    const [first_name, setFirstName] = useState("")
    const [last_name, setLastName] = useState("")
    const [email, setEmail] = useState("")
    const [homecourse, setHomeCourse] = useState("")
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [passwordcheck, setPasswordCheck] = useState("")
    const [profilePicture, setProfilePicture] = useState(null)
    const [thumbnailVisible, setThumbnailVisible] = useState(false);
    const [uploadedImageUrl, setUploadedImageUrl] = useState(null);
    const [profileData, setProfileData] = useState("")
  
    const { token } = useToken()

    const toggleThumbnailVisible = () => {
      setThumbnailVisible(!thumbnailVisible);
    };

    useEffect(() => {
        fetchAccountData();
      }, []);

    const fetchAccountData = async () => {
        const url = `${REACT_APP_API_HOST}/api/user-account`
        const response = await fetch(url, {
            "method": "GET",
            headers: { Authorization: `Bearer ${token}` }
        })

        if (response.ok) {
            const data = await response.json()
            setProfileData(data)
            console.log(data)
        }
    }

    const handleSubmit = async () => {
        try {
            if (!username || !password) {
                setErrorMessage('Please fill out all fields.');
              } else if (password !== passwordcheck) {
                setErrorMessage('Passwords do not match');
              } else if (profilePicture === null){
                setErrorMessage('Please choose Profile Picture');
              } else {
            const userInputs = {
              "first_name": first_name,
              "last_name": last_name,
              "email": email,
              "username": username,
              "password": password,
              "home_course": homecourse,
              "profile_pic_url": uploadedImageUrl
            };
            const url = `${REACT_APP_API_HOST}/api/accounts`;
            const response = await fetch(url,{
                method: "PUT",
                body: JSON.stringify(userInputs),
                headers: {
                    Authorization: `Bearer ${token}`,
                    "Content-Type": "application/json",
                },
            })
            if (response.status === 200) {
                navigation.navigate('Profile')
              } else {
                setLoading(false)
              }
          } 
        } catch (error) {
          setErrorMessage('An error occurred during updating');
        }
      };

      async function uploadImage(profilePicture) {
        const uri = profilePicture.assets[0]?.uri
        const fileName = uri.split('/').pop();
        const formData = new FormData();
        formData.append('file', {
          uri: profilePicture.assets[0]?.uri,
          type: profilePicture.assets[0]?.type,
          name: fileName
        });
        try {
          const response = await fetch('http://localhost:8000/api/upload-image', {
            method: 'POST',
            body: formData,
            headers: {
              'Content-Type': 'multipart/form-data',
            },
          });
    
          if (response.ok) {
            const data = await response.json();
            setUploadedImageUrl(data);
          } 
        } catch (error) {
          setErrorMessage('An error occurred during image upload');
        }
      };
        

      const pickImage = async () => {
        const result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
          allowsEditing: true,
          aspect: [4, 3],
          quality: 1,
        });
    
        if (!result.cancelled) {
          setProfilePicture(result);
          toggleThumbnailVisible()
          uploadImage(result)
        }
      };
      
        
      const pickImageAgain = async () => {
        const result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
          allowsEditing: true,
          aspect: [4, 3],
          quality: 1,
        });
    
        if (!result.cancelled) {
          setProfilePicture(result);
        }
      };

    const handleFirstnameChange = (text) => {
        setFirstName(text);
        setErrorMessage('');
      };
    
    const handleLastnameChange = (text) => {
        setLastName(text);
        setErrorMessage('');
      };
    
    const handleEmailChange = (text) => {
        setEmail(text);
        setErrorMessage('');
      };

    const handleUsernameChange = (text) => {
        setUsername(text);
        setErrorMessage('');
      };
    
    const handleHomecourseChange = (text) => {
        setHomeCourse(text);
        setErrorMessage('');
      };

    const handlePasswordChange = (text) => {
        setPassword(text);
        setErrorMessage('');
      };

    const handlePasswordCheckChange = (text) => {
        setPasswordCheck(text);
        setErrorMessage('');
      };

    return (
        <View style={styles.container}>
            <View style={styles.inputContainerupdate}>
            {!thumbnailVisible && (
              <View style={styles.profileimageContainer}>
                <Image source={require('ghi/assets/default_profile_img.jpg')} style={styles.defaultimg}/>
                  <Pressable onPress={pickImage} style={styles.profilepicbuttonupdate}>
                  <Text style={styles.profilebuttontextupdate}>CHOOSE PROFILE PICTURE</Text>
                </Pressable>
                </View>
                )}
                {thumbnailVisible && profilePicture && (
                <View style={styles.profileimageContainer}>
                  <Image
                    source={{ uri: profilePicture.assets[0].uri }}
                    style={styles.thumbnail}
                  />
                  <Pressable onPress={pickImageAgain} style={styles.profilepicbutton}>
                  <Text style={styles.profilebuttontextupdate}>CHOOSE ANOTHER IMAGE</Text>
                </Pressable>
                </View>
        )}
                <TextInput
                style={styles.regtextInput}
                placeholder={profileData.first_name} 
                onChangeText={handleFirstnameChange}
                />
                <TextInput
                style={styles.regtextInput}
                placeholder={profileData.last_name}
                onChangeText={handleLastnameChange}
                />
                <TextInput
                autoCapitalize='none'
                style={styles.regtextInput}
                placeholder={profileData.email}
                onChangeText={handleEmailChange}
                />
                <TextInput
                autoCapitalize='none'
                style={styles.regtextInput}
                placeholder={profileData.username}
                onChangeText={handleUsernameChange}
                />
                <TextInput
                style={styles.regtextInput}
                placeholder={profileData.home_course}
                onChangeText={handleHomecourseChange}
                />
                <TextInput
                autoCapitalize='none'
                style={styles.regtextInput}
                placeholder="Password" 
                secureTextEntry={true} 
                onChangeText={handlePasswordChange} 
                value={password} 
                />
                <TextInput
                autoCapitalize='none'
                style={styles.regtextInput}
                placeholder="Confirm Password" 
                secureTextEntry={true} 
                onChangeText={handlePasswordCheckChange} 
                value={passwordcheck} 
                />
                <View style={styles.buttoncontainerupdate}>
                {loading ? (
                    <ActivityIndicator size="large" color="#333c31" />
                    ) : (
                    <>
                    {errorMessage ? (
                        <View style={styles.errorcontainer}>
                        <Text style={styles.errorText}>{errorMessage}</Text>
                        </View>
                    ) : null}
                    <Pressable
                        onPress={handleSubmit}
                        style={styles.mediumbuttonupdate}
                        >
                        <Text style={styles.buttontextupdate}>Update</Text>
                    </Pressable>
                    </>
                )}
                <Pressable 
                    onPress={() => navigation.navigate('Profile')} 
                    style={styles.longbuttonupdate}
                    >
                    <Text style={styles.buttontextupdate}>Cancel</Text>    
                </Pressable>
                </View>
            </View>
        </View>
    )
}

export default UpdateProfile
