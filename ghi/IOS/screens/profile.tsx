import { View, Text, Image, Pressable, ScrollView, StyleSheet } from 'react-native'
import React, { useEffect, useState } from 'react'
import useToken from '../context/AuthContext';
import styles from './styles';
const REACT_APP_API_HOST = 'http://localhost:8000'

const Profile = ({ navigation }) => {
    const [profileData, setProfileData] = useState("")
    const [nineScorecards, setNineScorecards] = useState([])
    const [bestScore, setBestScore] = useState(1000)
    const [totalRounds, setTotalRounds] = useState(0)
    const [PPHperRound, setPPHPerRound] = useState(0)
    const [avgFIRPerRound, setAvgFirPerRound] = useState(0)
    const [avgGirPerRound, setAvgGirPerRound] = useState(0)
    const [avgScore, setAvgScore] = useState(0)
    const { token } = useToken()


    useEffect(() => {
        fetchAccountData();
        fetchNineScorecards()
      }, [token]);

    const fetchAccountData = async () => {
        const url = `${REACT_APP_API_HOST}/api/user-account`
        const response = await fetch(url, {
            "method": "GET",
            headers: { Authorization: `Bearer ${token}` }
        })

        if (response.ok) {
            const data = await response.json()
            setProfileData(data)
        }
    }
    const fetchNineScorecards = async () => {
        const url = `${REACT_APP_API_HOST}/api/user/nine-hole-scores`
        const response = await fetch(url, {
            "method": "GET",
            headers: { Authorization: `Bearer ${token}` }
        })

        if (response.ok) {
            const data = await response.json()
            setNineScorecards(data)
            let totalFir = 0
            let totalGir = 0
            let totalPutts = 0
            let totalScore = 0
            let totalRounds1 = 0
            for (let oneRound of data) {
                totalFir = totalFir + oneRound["avg_fir"]
                totalGir = totalGir + oneRound["avg_gir"]
                totalPutts = totalPutts + oneRound["avg_putts"]
                totalScore = totalScore + oneRound["score"]
                totalRounds1 = totalRounds1 + 1
                }
            setAvgFirPerRound(totalFir/data.length)
            setAvgGirPerRound(totalGir/data.length)
            setPPHPerRound(totalPutts/data.length)
            setAvgScore(totalScore)
            setTotalRounds(totalRounds1)
        }
    }

    return (
        <View style={styles.profilescreen}>
            <View style={styles.profileSafeAreaExtend}>
                <Pressable 
                style={styles.updateprofilebutton}
                onPress={() => navigation.navigate('Update')}
                >
                    <Text style={styles.updateprofilebuttontext}>Update</Text>
                </Pressable>
            </View>
            <View style={styles.profileContainer}>
                <Image source={{ uri: profileData.profile_pic_url }} style={styles.profileImage} />
                <View style={styles.profileinfo}>
                    <Text style={styles.profilename}>{profileData.first_name} {profileData.last_name}</Text>
                    <Text style={styles.profileusername}>{profileData.username}</Text>
                    <Text style={styles.profilehomecourse}>{profileData.home_course}</Text>
                </View>
            </View>
            <View style={styles.profileroundcontainer}>
                <View style={styles.profileroundheader}>
                    <Text style={styles.profileroundtext}>Most  Recent  Rounds</Text>
                </View>
                <View style={styles.container1}>
                    <ScrollView style={styles.scrollviewrounds}>
                    {nineScorecards.map((round) => (
                        <View key={round.id} style={styles.roundinfocontainer}>
                            <View style={styles.roundinfosubcontSCORE}>
                                <View style={styles.roundinfoheaderSCORE}>
                                    <Text style={styles.headertext}>Score</Text>
                                </View>
                                <Text style={styles.rounddataScore}>{round.score}</Text>
                            </View>
                            <View style={styles.roundinfosubcont}>
                                <View style={styles.roundinfoheader}>
                                    <Text style={styles.headertext}>Course</Text>
                                </View>
                                <Text style={styles.rounddataCourse}>{round.course_name}</Text>
                            </View>
                            <View style={styles.roundinfoGIRFIRsubcont}>
                                <View style={styles.roundinfoheader}>
                                    <Text style={styles.headertext}>Par Diff</Text>
                                </View>
                                {round.par_differential > 0 && (
                                    <Text style={styles.rounddataScore}>+ {round.par_differential}</Text>
                                )}
                                {round.par_differential <= 0 && (
                                    <Text style={styles.rounddataScore}> {round.par_differential} </Text>
                                )}
                            </View>
                            <View style={styles.roundinfoGIRFIRsubcont}>
                                <View style={styles.roundinfoheader}>
                                    <Text style={styles.headertext}>PPH</Text>
                                </View>
                                <Text style={styles.rounddataScore}>{round.avg_putts}</Text>
                            </View>
                            <View style={styles.roundinfoGIRFIRsubcont}>
                                <View style={styles.roundinfoheader}>
                                    <Text style={styles.headertext}>FIR</Text>
                                </View>
                                <Text style={styles.rounddataScore}>{Math.round(round.avg_fir*100)}%</Text>
                            </View>
                            <View style={styles.roundinfoGIRFIRsubcont}>
                                <View style={styles.roundinfoheader}>
                                    <Text style={styles.headertext}>GIR</Text>
                                </View>
                                <Text style={styles.rounddataScore}>{Math.round(round.avg_gir*100)}%</Text>
                            </View>
                        </View>
                    ))}
                    </ScrollView>
                </View>
                <View style={styles.statscont}>
                        <View style={styles.statsheader}>
                                <Text style={styles.profileroundtext}>Stats</Text>
                        </View>
                        <View style={styles.statsgrid}>
                                <View style={styles.statsleft}>
                                    <Text style={styles.statslefttext}>Avg Nine Hole Score:</Text>
                                    <Text style={styles.statslefttext}>Avg PPH Per Round:</Text>
                                    <Text style={styles.statslefttext}>Avg FIR Per Round:</Text>
                                    <Text style={styles.statslefttext}>Avg GIR Per Round:</Text>
                                </View>
                                <View style={styles.statsright}>
                                    <Text style={styles.statsrighttext}>{Math.round(avgScore/totalRounds)}</Text>
                                    <Text style={styles.statsrighttext}>{Math.round(PPHperRound * 100) / 100}</Text>
                                    <Text style={styles.statsrighttext}>{Math.round(avgFIRPerRound * 100)}%</Text>
                                    <Text style={styles.statsrighttext}>{Math.round(avgGirPerRound * 100)}%</Text>
                                </View>
                        </View>
                    </View>
            </View>
        </View>
    )
}

export default Profile