from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import accounts, ninescorecard, eighteenscorecard, friend_requests
import os
from authenticator import authenticator

app = FastAPI()

origins = ["http://localhost:8000", "http://localhost:3000"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00",
        }
    }


app.include_router(authenticator.router, tags=["login/logout"])
app.include_router(accounts.router, tags=["Accounts"])
app.include_router(ninescorecard.router, tags=["NineScoreCard"])
app.include_router(eighteenscorecard.router, tags=["EighteenScoreCard"])
app.include_router(friend_requests.router, tags=["Friend Requests"])
