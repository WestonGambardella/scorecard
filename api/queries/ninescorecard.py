from pydantic import BaseModel
from queries.pool import pool
from datetime import date

class nineScoreIn(BaseModel):
    course_name: str
    course_city: str
    course_state: str
    tees: str
    par_differential: int
    avg_putts: float
    avg_gir: float
    avg_fir: float
    score: int
   

class nineScoreOut(BaseModel):
    course_name: str
    course_city: str
    course_state: str
    tees: str
    par_differential: int
    avg_putts: float
    avg_gir: float
    avg_fir: float
    score: int
    id: int
    round_date: date
    player_id: int


class nineScoreRepo:
    def create_ninescore(
            self,
            data: nineScoreIn,
            player_id,
            round_date,
            ) -> nineScoreOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    data.course_name,
                    data.course_city,
                    data.course_state,
                    data.tees,
                    data.par_differential,
                    data.avg_fir,
                    data.avg_gir,
                    data.avg_putts,
                    data.score,
                    player_id,
                    round_date,
                ]
                result = cur.execute(
                    """
                        INSERT INTO ninescorecard (
                            course_name,
                            course_city,
                            course_state,
                            tees,
                            par_differential,
                            avg_putts,
                            avg_gir,
                            avg_fir,
                            score,
                            player_id,
                            round_date
                            )
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                        RETURNING id, course_name, course_city, course_state, tees, par_differential, avg_putts, avg_gir, avg_fir, score, player_id, round_date
                    """,
                    params,
                )
                record = None
                row = result.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(result.description):
                        record[column.name] = row[i]
                return nineScoreOut(**record)


    def get_all_nine_scores_user(self, player_id) -> list[nineScoreOut]:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                            SELECT *
                            FROM ninescorecard
                            WHERE player_id = %s
                            ORDER BY id;
                        """,
                        [player_id],
                    )
                    result= []
                    for record in cur:
                        ninescorecard = nineScoreOut(
                            id=record[0],
                            course_name=record[1],
                            course_city=record[2],
                            course_state=record[3],
                            tees=record[4],
                            par_differential=record[5],
                            avg_gir=record[6],
                            avg_fir=record[7],
                            avg_putts=record[8],
                            score=record[9],
                            player_id=record[10],
                            round_date=record[11],
                        )
                        result.append(ninescorecard)
                    return result


    def get_all_nine_scorecards(self) -> list[nineScoreOut]:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                            SELECT *
                            FROM ninescorecard
                            ORDER BY id;
                        """
                    )
                    result= []
                    for record in cur:
                        ninescorecard = nineScoreOut(
                            id = record[0],
                            player_id=record[1],
                            course_name=record[2],
                            course_city=record[3],
                            course_state=record[4],
                            round_date=record[5],
                            tees=record[6],
                            par_differential=record[7],
                            avg_putts=record[8],
                            avg_fairway=record[9],
                            avg_gir=record[10],
                            avg_fir=record[11],
                            score=record[12],
                        )
                        result.append(ninescorecard)
                    return result
