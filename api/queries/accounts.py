from pydantic import BaseModel
from queries.pool import pool
from typing import Union
from datetime import datetime


class Error(BaseModel):
    pass


class DuplicateAccountError(ValueError):
    pass


class Account(BaseModel):
    email: str
    first_name: str
    home_course: str
    last_name: str
    password: str
    username: str
    

class AccountOut(BaseModel):
    first_name: str
    last_name: str
    email: str
    username: str
    home_course: str
    id: int
    created: str
    is_active: bool


class AccountOutWithHashedPassword(AccountOut):
    hashed_password: str

class AccountRepo:
    def get_all(self) -> Union[Error, list[AccountOut]]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT *
                    FROM accounts
                    ORDER BY id;
                    """,
                )
                result = []
                for record in cur:
                    account = AccountOut(
                        id=record[0],
                        first_name=record[1],
                        last_name=record[2],
                        email=record[3],
                        username=record[4],
                        home_course=record[5],
                        created=str(record[7]),
                        is_active=record[8]                  
                        )
                    result.append(account)
                return result

    def get_one_account(self, id: int) -> AccountOutWithHashedPassword:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT *
                    FROM accounts
                    WHERE id = %s;
                    """,
                    [id],
                )
                try:
                    record = None
                    for row in cur.fetchall():
                        record = {}
                        for i, column in enumerate(cur.description):
                            record[column.name] = row[i]
                    return AccountOutWithHashedPassword(**record)
                except Exception:
                    return {"message": "Could not get account"}


    def get_account(self, username: str) -> AccountOutWithHashedPassword:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                    SELECT *
                    FROM accounts
                    WHERE username = %s;
                    """,
                    [username],
                )
                try:
                    record = None
                    for row in cur.fetchall():
                        record = {}
                        for i, column in enumerate(cur.description):
                            record[column.name] = row[i]
                    return AccountOutWithHashedPassword(**record)
                except Exception:
                    return {"message": "Could not get account"}


    def create_account(
        self, data, hashed_password
    ) -> AccountOut:
        now = datetime.now()
        timestamp = now.strftime("%Y-%m-%dT%H:%M:%SZ")
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    data.email,
                    data.first_name,
                    data.home_course,
                    data.last_name,
                    data.username,
                    hashed_password,
                    timestamp
                ]
                cur.execute(
                    """
                    INSERT INTO accounts (email, first_name, home_course, last_name, username, hashed_password, created, is_active)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, true)
                    RETURNING id, first_name, last_name, email, username, home_course, created, is_active
                    """,
                    params,
                )
                record = None
                row = cur.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(cur.description):
                        record[column.name] = row[i]
                return AccountOut(**record)

    def update_account(
        self, account_id: int, data: Account
    ) -> Union[AccountOut, Error]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                params = [
                    data.first_name,
                    data.last_name,
                    data.email,
                    data.username,
                    data.home_course,
                    account_id,
                ]
                db.execute(
                    """
                    UPDATE accounts
                    SET first_name = %s, last_name = %s, email = %s, username = %s, home_course = %s
                    WHERE id = %s
                    RETURNING id, first_name, last_name, email, username, home_course
                    """,
                    params,
                )
                record = None
                row = db.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(db.description):
                        record[column.name] = row[i]
                return AccountOut(**record)

    def delete_account(self, account_id: int) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM accounts
                    WHERE id = %s
                    """,
                    [account_id],
                )
                return True
