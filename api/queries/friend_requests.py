from pydantic import BaseModel
from queries.pool import pool

class friendRequestIn(BaseModel):
    receiver: int

class friendRequestOut(friendRequestIn):
    sender: int
    request_status: str
    id: int


class friendRequestRepo:
    def create_request(
            self,
            sender_id,
            request_status,
            data: friendRequestIn,
            ) -> friendRequestOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    sender_id,
                    data.receiver,
                    request_status,
                ]
                result = cur.execute(
                    """
                        INSERT INTO friends (
                            sender,
                            receiver,
                            request_status
                            )
                        VALUES (%s, %s, %s)
                        RETURNING id, sender, receiver, request_status
                    """,
                    params,
                )
                record = None
                row = result.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(result.description):
                        record[column.name] = row[i]
                return friendRequestOut(**record)
            
    def get_friend_requests(self, user_id) -> list[friendRequestOut]:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(
                    """
                        SELECT *
                        FROM friends
                        WHERE receiver = %s
                        ORDER BY id;
                    """,
                    [user_id],
                )
                result= []
                for record in cur:
                    request = friendRequestOut(
                        id=record[0],
                        sender=record[1],
                        receiver=record[2],
                        request_status=record[3],
                    )
                    result.append(request)
                return result

    def accept_friend_request(self, accepted, request_id, receiver_id) -> friendRequestOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                        UPDATE friends
                        SET request_status = %s
                        WHERE id = %s AND receiver = %s
                        RETURNING id, sender, receiver, request_status
                    """,
                    [accepted, request_id, receiver_id],
                )
                record = None
                row = result.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(result.description):
                        record[column.name] = row[i]
                return friendRequestOut(**record)
                    
    def deny_friend_request(self, deny, request_id, receiver_id) -> friendRequestOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                result = cur.execute(
                    """
                        UPDATE friends
                        SET request_status = %s
                        WHERE id = %s AND receiver = %s
                        RETURNING id, sender, receiver, request_status
                    """,
                    [deny, request_id, receiver_id],
                )
                record = None
                row = result.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(result.description):
                        record[column.name] = row[i]
                return friendRequestOut(**record)
# def create_ninescore(
#             self,
#             data: nineScoreIn,
#             player_id,
#             round_date,
#             ) -> nineScoreOut:
#         with pool.connection() as conn:
#             with conn.cursor() as cur:
#                 params = [
#                     data.course_name,
#                     data.course_city,
#                     data.course_state,
#                     data.tees,
#                     data.par_differential,
#                     data.avg_fir,
#                     data.avg_gir,
#                     data.avg_putts,
#                     data.score,
#                     player_id,
#                     round_date,
#                 ]
#                 result = cur.execute(
#                     """
#                         INSERT INTO ninescorecard (
#                             course_name,
#                             course_city,
#                             course_state,
#                             tees,
#                             par_differential,
#                             avg_putts,
#                             avg_gir,
#                             avg_fir,
#                             score,
#                             player_id,
#                             round_date
#                             )
#                         VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
#                         RETURNING id, course_name, course_city, course_state, tees, par_differential, avg_putts, avg_gir, avg_fir, score, player_id, round_date
#                     """,
#                     params,
#                 )
#                 record = None
#                 row = result.fetchone()
#                 if row is not None:
#                     record = {}
#                     for i, column in enumerate(result.description):
#                         record[column.name] = row[i]
#                 return nineScoreOut(**record)