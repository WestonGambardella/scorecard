from pydantic import BaseModel
from queries.pool import pool
from datetime import date

class eighteenScoreIn(BaseModel):
    player_id: int
    course_name: str
    course_city: str
    course_state: str
    round_date: date
    tees: str
    hole1par: int
    hole1fairway: bool
    hole1gir: bool
    hole1fir: bool
    hole1putts: int
    hole1score: int
    hole2par: int
    hole2fairway: bool
    hole2gir: bool
    hole2fir: bool
    hole2putts: int
    hole2score: int
    hole3par: int
    hole3fairway: bool
    hole3gir: bool
    hole3fir: bool
    hole3putts: int
    hole3score: int
    hole4par: int
    hole4fairway: bool
    hole4gir: bool
    hole4fir: bool
    hole4putts: int
    hole4score: int
    hole5par: int
    hole5fairway: bool
    hole5gir: bool
    hole5fir: bool
    hole5putts: int
    hole5score: int
    hole6par: int
    hole6fairway: bool
    hole6gir: bool
    hole6fir: bool
    hole6putts: int
    hole6score: int
    hole7par: int
    hole7fairway: bool
    hole7gir: bool
    hole7fir: bool
    hole7putts: int
    hole7score: int
    hole8par: int
    hole8fairway: bool
    hole8gir: bool
    hole8fir: bool
    hole8putts: int
    hole8score: int
    hole9par: int
    hole9fairway: bool
    hole9gir: bool
    hole9fir: bool
    hole9putts: int
    hole9score: int
    hole10par: int
    hole10fairway: bool
    hole10gir: bool
    hole10fir: bool
    hole10putts: int
    hole10score: int
    hole11par: int
    hole11fairway: bool
    hole11gir: bool
    hole11fir: bool
    hole11putts: int
    hole11score: int
    hole12par: int
    hole12fairway: bool
    hole12gir: bool
    hole12fir: bool
    hole12putts: int
    hole12score: int
    hole13par: int
    hole13fairway: bool
    hole13gir: bool
    hole13fir: bool
    hole13putts: int
    hole13score: int
    hole14par: int
    hole14fairway: bool
    hole14gir: bool
    hole14fir: bool
    hole14putts: int
    hole14score: int
    hole15par: int
    hole15fairway: bool
    hole15gir: bool
    hole15fir: bool
    hole15putts: int
    hole15score: int
    hole16par: int
    hole16fairway: bool
    hole16gir: bool
    hole16fir: bool
    hole16putts: int
    hole16score: int
    hole17par: int
    hole17fairway: bool
    hole17gir: bool
    hole17fir: bool
    hole17putts: int
    hole17score: int
    hole18par: int
    hole18fairway: bool
    hole18gir: bool
    hole18fir: bool
    hole18putts: int
    hole18score: int

class eighteenScoreOut(eighteenScoreIn):
    id: int
    avg_puttsfrontnine: float
    avg_fairwayfrontnine: float
    avg_girfrontnine: float
    avg_firfrontnine: float
    avg_puttsbacknine: float
    avg_fairwaybacknine: float
    avg_girbacknine: float
    avg_firbacknine: float
    frontninescore: int
    backninescore: int
    score: int


class eighteenScoreRepo:
    def create_eighteenscore(
            self,
            data: eighteenScoreIn,
            player_id,
            avg_puttsfrontnine,
            avg_puttsbacknine,
            avg_fairwayfrontnine,
            avg_fairwaybacknine,
            avg_girfrontnine,
            avg_girbacknine,
            avg_firfrontnine,
            avg_firbacknine,
            frontninescore,
            backninescore,
            score
    ) -> eighteenScoreOut:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [
                    player_id,
                    data.course_name,
                    data.course_city,
                    data.course_state,
                    data.round_date,
                    data.tees,
                    data.hole1par,
                    data.hole1fairway,
                    data.hole1gir,
                    data.hole1fir,
                    data.hole1putts,
                    data.hole1score,
                    data.hole2par,
                    data.hole2fairway,
                    data.hole2gir,
                    data.hole2fir,
                    data.hole2putts,
                    data.hole2score,
                    data.hole3par,
                    data.hole3fairway,
                    data.hole3gir,
                    data.hole3fir,
                    data.hole3putts,
                    data.hole3score,
                    data.hole4par,
                    data.hole4fairway,
                    data.hole4gir,
                    data.hole4fir,
                    data.hole4putts,
                    data.hole4score,
                    data.hole5par,
                    data.hole5fairway,
                    data.hole5gir,
                    data.hole5fir,
                    data.hole5putts,
                    data.hole5score,
                    data.hole6par,
                    data.hole6fairway,
                    data.hole6gir,
                    data.hole6fir,
                    data.hole6putts,
                    data.hole6score,
                    data.hole7par,
                    data.hole7fairway,
                    data.hole7gir,
                    data.hole7fir,
                    data.hole7putts,
                    data.hole7score,
                    data.hole8par,
                    data.hole8fairway,
                    data.hole8gir,
                    data.hole8fir,
                    data.hole8putts,
                    data.hole8score,
                    data.hole9par,
                    data.hole9fairway,
                    data.hole9gir,
                    data.hole9fir,
                    data.hole9putts,
                    data.hole9score,
                    data.hole10par,
                    data.hole10fairway,
                    data.hole10gir,
                    data.hole10fir,
                    data.hole10putts,
                    data.hole10score,
                    data.hole11par,
                    data.hole11fairway,
                    data.hole11gir,
                    data.hole11fir,
                    data.hole11putts,
                    data.hole11score,
                    data.hole12par,
                    data.hole12fairway,
                    data.hole12gir,
                    data.hole12fir,
                    data.hole12putts,
                    data.hole12score,
                    data.hole13par,
                    data.hole13fairway,
                    data.hole13gir,
                    data.hole13fir,
                    data.hole13putts,
                    data.hole13score,
                    data.hole14par,
                    data.hole14fairway,
                    data.hole14gir,
                    data.hole14fir,
                    data.hole14putts,
                    data.hole14score,
                    data.hole15par,
                    data.hole15fairway,
                    data.hole15gir,
                    data.hole15fir,
                    data.hole15putts,
                    data.hole15score,
                    data.hole16par,
                    data.hole16fairway,
                    data.hole16gir,
                    data.hole16fir,
                    data.hole16putts,
                    data.hole16score,
                    data.hole17par,
                    data.hole17fairway,
                    data.hole17gir,
                    data.hole17fir,
                    data.hole17putts,
                    data.hole17score,
                    data.hole18par,
                    data.hole18fairway,
                    data.hole18gir,
                    data.hole18fir,
                    data.hole18putts,
                    data.hole18score,
                    avg_puttsfrontnine,
                    avg_fairwayfrontnine,
                    avg_girfrontnine,
                    avg_firfrontnine,
                    avg_puttsbacknine,
                    avg_fairwaybacknine,
                    avg_girbacknine,
                    avg_firbacknine,
                    frontninescore,
                    backninescore,
                    score
                ]
                result = cur.execute(
                    """
                        INSERT INTO eighteenscorecard (
                            player_id,
                            course_name,
                            course_city,
                            course_state,
                            round_date,
                            tees,
                            hole1par,
                            hole1fairway,
                            hole1gir,
                            hole1fir,
                            hole1putts,
                            hole1score,
                            hole2par,
                            hole2fairway,
                            hole2gir,
                            hole2fir,
                            hole2putts,
                            hole2score,
                            hole3par,
                            hole3fairway,
                            hole3gir,
                            hole3fir,
                            hole3putts,
                            hole3score,
                            hole4par,
                            hole4fairway,
                            hole4gir,
                            hole4fir,
                            hole4putts,
                            hole4score,
                            hole5par,
                            hole5fairway,
                            hole5gir,
                            hole5fir,
                            hole5putts,
                            hole5score,
                            hole6par,
                            hole6fairway,
                            hole6gir,
                            hole6fir,
                            hole6putts,
                            hole6score,
                            hole7par,
                            hole7fairway,
                            hole7gir,
                            hole7fir,
                            hole7putts,
                            hole7score,
                            hole8par,
                            hole8fairway,
                            hole8gir,
                            hole8fir,
                            hole8putts,
                            hole8score,
                            hole9par,
                            hole9fairway,
                            hole9gir,
                            hole9fir,
                            hole9putts,
                            hole9score,
                            hole10par,
                            hole10fairway,
                            hole10gir,
                            hole10fir,
                            hole10putts,
                            hole10score,
                            hole11par,
                            hole11fairway,
                            hole11gir,
                            hole11fir,
                            hole11putts,
                            hole11score,
                            hole12par,
                            hole12fairway,
                            hole12gir,
                            hole12fir,
                            hole12putts,
                            hole12score,
                            hole13par,
                            hole13fairway,
                            hole13gir,
                            hole13fir,
                            hole13putts,
                            hole13score,
                            hole14par,
                            hole14fairway,
                            hole14gir,
                            hole14fir,
                            hole14putts,
                            hole14score,
                            hole15par,
                            hole15fairway,
                            hole15gir,
                            hole15fir,
                            hole15putts,
                            hole15score,
                            hole16par,
                            hole16fairway,
                            hole16gir,
                            hole16fir,
                            hole16putts,
                            hole16score,
                            hole17par,
                            hole17fairway,
                            hole17gir,
                            hole17fir,
                            hole17putts,
                            hole17score,
                            hole18par,
                            hole18fairway,
                            hole18gir,
                            hole18fir,
                            hole18putts,
                            hole18score,
                            avg_puttsfrontnine,
                            avg_fairwayfrontnine,
                            avg_girfrontnine,
                            avg_firfrontnine,
                            avg_puttsbacknine,
                            avg_fairwaybacknine,
                            avg_girbacknine,
                            avg_firbacknine,
                            frontninescore,
                            backninescore,
                            score
                            )
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                        RETURNING *
                    """,
                    params,
                )
                record = None
                row = result.fetchone()
                if row is not None:
                    record = {}
                    for i, column in enumerate(result.description):
                        record[column.name] = row[i]
                return eighteenScoreOut(**record)


    def get_all_eighteen_scores_user(self, player_id: int) -> list[eighteenScoreOut]:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                            SELECT *
                            FROM eighteenscorecard
                            WHERE player_id = %s
                            ORDER BY id;
                        """,
                        [player_id],
                    )
                    result= []
                    for record in cur:
                        eighteenscorecard = eighteenScoreOut(
                            id = record[0],
                            player_id=record[1],
                            course_name=record[2],
                            course_city=record[3],
                            course_state=record[4],
                            round_date=record[5],
                            tees=record[6],
                            hole1par=record[7],
                            hole1fairway=record[8],
                            hole1gir= record[9],
                            hole1fir= record[10],
                            hole1putts=record[11],
                            hole1score=record[12],
                            hole2par=record[13],
                            hole2fairway=record[14],
                            hole2gir= record[15],
                            hole2fir= record[16],
                            hole2putts=record[17],
                            hole2score=record[18],
                            hole3par=record[19],
                            hole3fairway=record[20],
                            hole3gir= record[21],
                            hole3fir= record[22],
                            hole3putts=record[23],
                            hole3score=record[24],
                            hole4par=record[25],
                            hole4fairway=record[26],
                            hole4gir= record[27],
                            hole4fir= record[28],
                            hole4putts=record[29],
                            hole4score=record[30],
                            hole5par=record[31],
                            hole5fairway=record[32],
                            hole5gir= record[33],
                            hole5fir= record[34],
                            hole5putts=record[35],
                            hole5score=record[36],
                            hole6par=record[37],
                            hole6fairway=record[38],
                            hole6gir= record[39],
                            hole6fir= record[40],
                            hole6putts=record[41],
                            hole6score=record[42],
                            hole7par=record[43],
                            hole7fairway=record[44],
                            hole7gir= record[45],
                            hole7fir= record[46],
                            hole7putts=record[47],
                            hole7score=record[48],
                            hole8par=record[49],
                            hole8fairway=record[50],
                            hole8gir= record[51],
                            hole8fir= record[52],
                            hole8putts=record[53],
                            hole8score=record[54],
                            hole9par=record[55],
                            hole9fairway=record[55],
                            hole9gir= record[56],
                            hole9fir= record[57],
                            hole9putts=record[58],
                            hole9score=record[59],
                            hole10par=record[60],
                            hole10fairway=record[61],
                            hole10gir= record[62],
                            hole10fir= record[63],
                            hole10putts=record[64],
                            hole10score=record[65],
                            hole11par=record[66],
                            hole11fairway=record[67],
                            hole11gir= record[68],
                            hole11fir= record[69],
                            hole11putts=record[70],
                            hole11score=record[71],
                            hole12par=record[72],
                            hole12fairway=record[73],
                            hole12gir= record[74],
                            hole12fir= record[75],
                            hole12putts=record[76],
                            hole12score=record[77],
                            hole13par=record[78],
                            hole13fairway=record[79],
                            hole13gir= record[80],
                            hole13fir= record[81],
                            hole13putts=record[82],
                            hole13score=record[83],
                            hole14par=record[84],
                            hole14fairway=record[85],
                            hole14gir= record[86],
                            hole14fir= record[87],
                            hole14putts=record[88],
                            hole14score=record[89],
                            hole15par=record[90],
                            hole15fairway=record[91],
                            hole15gir= record[92],
                            hole15fir= record[93],
                            hole15putts=record[94],
                            hole15score=record[95],
                            hole16par=record[96],
                            hole16fairway=record[97],
                            hole16gir= record[98],
                            hole16fir= record[99],
                            hole16putts=record[100],
                            hole16score=record[101],
                            hole17par=record[102],
                            hole17fairway=record[103],
                            hole17gir= record[104],
                            hole17fir= record[105],
                            hole17putts=record[106],
                            hole17score=record[107],
                            hole18par=record[108],
                            hole18fairway=record[109],
                            hole18gir= record[110],
                            hole18fir= record[111],
                            hole18putts=record[112],
                            hole18score=record[113],
                            avg_puttsfrontnine=record[114],
                            avg_fairwayfrontnine=record[115],
                            avg_girfrontnine=record[116],
                            avg_firfrontnine=record[117],
                            avg_puttsbacknine=record[118],
                            avg_fairwaybacknine=record[119],
                            avg_girbacknine=record[120],
                            avg_firbacknine=record[121],
                            frontninescore=record[122],
                            backninescore=record[123],
                            score=record[124]
                        )
                        result.append(eighteenscorecard)
                    return result


    def get_all_eighteen_scorecards(self) -> list[eighteenScoreOut]:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                            SELECT *
                            FROM eighteenscorecard
                            ORDER BY id;
                        """
                    )
                    result= []
                    for record in cur:
                        eighteenscorecard = eighteenScoreOut(
                            id = record[0],
                            player_id=record[1],
                            course_name=record[2],
                            course_city=record[3],
                            course_state=record[4],
                            round_date=record[5],
                            tees=record[6],
                            hole1par=record[7],
                            hole1fairway=record[8],
                            hole1gir= record[9],
                            hole1fir= record[10],
                            hole1putts=record[11],
                            hole1score=record[12],
                            hole2par=record[13],
                            hole2fairway=record[14],
                            hole2gir= record[15],
                            hole2fir= record[16],
                            hole2putts=record[17],
                            hole2score=record[18],
                            hole3par=record[19],
                            hole3fairway=record[20],
                            hole3gir= record[21],
                            hole3fir= record[22],
                            hole3putts=record[23],
                            hole3score=record[24],
                            hole4par=record[25],
                            hole4fairway=record[26],
                            hole4gir= record[27],
                            hole4fir= record[28],
                            hole4putts=record[29],
                            hole4score=record[30],
                            hole5par=record[31],
                            hole5fairway=record[32],
                            hole5gir= record[33],
                            hole5fir= record[34],
                            hole5putts=record[35],
                            hole5score=record[36],
                            hole6par=record[37],
                            hole6fairway=record[38],
                            hole6gir= record[39],
                            hole6fir= record[40],
                            hole6putts=record[41],
                            hole6score=record[42],
                            hole7par=record[43],
                            hole7fairway=record[44],
                            hole7gir= record[45],
                            hole7fir= record[46],
                            hole7putts=record[47],
                            hole7score=record[48],
                            hole8par=record[49],
                            hole8fairway=record[50],
                            hole8gir= record[51],
                            hole8fir= record[52],
                            hole8putts=record[53],
                            hole8score=record[54],
                            hole9par=record[55],
                            hole9fairway=record[55],
                            hole9gir= record[56],
                            hole9fir= record[57],
                            hole9putts=record[58],
                            hole9score=record[59],
                            hole10par=record[60],
                            hole10fairway=record[61],
                            hole10gir= record[62],
                            hole10fir= record[63],
                            hole10putts=record[64],
                            hole10score=record[65],
                            hole11par=record[66],
                            hole11fairway=record[67],
                            hole11gir= record[68],
                            hole11fir= record[69],
                            hole11putts=record[70],
                            hole11score=record[71],
                            hole12par=record[72],
                            hole12fairway=record[73],
                            hole12gir= record[74],
                            hole12fir= record[75],
                            hole12putts=record[76],
                            hole12score=record[77],
                            hole13par=record[78],
                            hole13fairway=record[79],
                            hole13gir= record[80],
                            hole13fir= record[81],
                            hole13putts=record[82],
                            hole13score=record[83],
                            hole14par=record[84],
                            hole14fairway=record[85],
                            hole14gir= record[86],
                            hole14fir= record[87],
                            hole14putts=record[88],
                            hole14score=record[89],
                            hole15par=record[90],
                            hole15fairway=record[91],
                            hole15gir= record[92],
                            hole15fir= record[93],
                            hole15putts=record[94],
                            hole15score=record[95],
                            hole16par=record[96],
                            hole16fairway=record[97],
                            hole16gir= record[98],
                            hole16fir= record[99],
                            hole16putts=record[100],
                            hole16score=record[101],
                            hole17par=record[102],
                            hole17fairway=record[103],
                            hole17gir= record[104],
                            hole17fir= record[105],
                            hole17putts=record[106],
                            hole17score=record[107],
                            hole18par=record[108],
                            hole18fairway=record[109],
                            hole18gir= record[110],
                            hole18fir= record[111],
                            hole18putts=record[112],
                            hole18score=record[113],
                            avg_puttsfrontnine=record[114],
                            avg_fairwayfrontnine=record[115],
                            avg_girfrontnine=record[116],
                            avg_firfrontnine=record[117],
                            avg_puttsbacknine=record[118],
                            avg_fairwaybacknine=record[119],
                            avg_girbacknine=record[120],
                            avg_firbacknine=record[121],
                            frontninescore=record[122],
                            backninescore=record[123],
                            score=record[124]
                        )
                        result.append(eighteenscorecard)
                    return result
