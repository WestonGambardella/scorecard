import boto3
from botocore.exceptions import ClientError

from fastapi import (
    Depends,
    APIRouter,
    Request,
    UploadFile,
)
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator

from pydantic import BaseModel

from queries.friend_requests import (
    friendRequestIn,
    friendRequestOut,
    friendRequestRepo,
)

router = APIRouter()


@router.post("/api/send-friend-request", response_model=friendRequestOut)
def create_friend_request(
    info: friendRequestIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: friendRequestRepo = Depends(),
) -> friendRequestOut:
    sender_id = account_data["id"]
    request_status = "pending"
    return repo.create_request(sender_id, request_status, info)

@router.get("/api/get-pending-requests", response_model=list[friendRequestOut])
async def get_friend_requests(
    repo: friendRequestRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
):
    user_id = account_data["id"]
    return repo.get_friend_requests(user_id)

@router.put("/api/friend-requests/{request_id}/accept", response_model=friendRequestOut)
def accept_friend_request(
    request_id: int,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: friendRequestRepo = Depends(),
) -> friendRequestOut:
    receiver_id = account_data["id"]
    accepted = "accepted"
    return repo.accept_friend_request(accepted, request_id, receiver_id)

@router.put("/api/friend-requests/{request_id}/deny", response_model=friendRequestOut)
def deny_friend_request(
    request_id: int,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: friendRequestRepo = Depends(),
) -> friendRequestOut:
    receiver_id = account_data["id"]
    denied = "denied"
    return repo.deny_friend_request(denied, request_id, receiver_id)