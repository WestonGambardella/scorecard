from fastapi import Depends, APIRouter, HTTPException
from authenticator import authenticator
import os
import requests
from queries.eighteenscorecard import eighteenScoreIn, eighteenScoreOut, eighteenScoreRepo

router = APIRouter()


@router.get("/api/eighteen-hole-scores", response_model=list[eighteenScoreOut])
async def get_all_eighteenscorecards(
    repo: eighteenScoreRepo = Depends(),
):
    return repo.get_all_eighteen_scorecards()


@router.get("/api/user/eighteen-hole-scores", response_model=list[eighteenScoreOut])
async def get_all_eighteenscorecards_from_user(
    repo: eighteenScoreRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
):
    player_id = account_data["id"]
    return repo.get_all_eighteen_scores_user(player_id)



@router.post("/api/create_eighteenscorecard", response_model=eighteenScoreOut)
async def create_eighteenscorecard(
    incoming: eighteenScoreIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: eighteenScoreRepo = Depends()
) -> eighteenScoreOut:
    avg_puttsfrontnine = (
        incoming.hole10putts+
        incoming.hole11putts+
        incoming.hole12putts+
        incoming.hole13putts+
        incoming.hole14putts+
        incoming.hole15putts+
        incoming.hole16putts+
        incoming.hole17putts+
        incoming.hole18putts
        ) / 9

    avg_puttsbacknine = (
        incoming.hole10putts+
        incoming.hole11putts+
        incoming.hole12putts+
        incoming.hole13putts+
        incoming.hole14putts+
        incoming.hole15putts+
        incoming.hole16putts+
        incoming.hole17putts+
        incoming.hole18putts
        ) / 9

    frontninescore = (
        incoming.hole1score+
        incoming.hole2score+
        incoming.hole3score+
        incoming.hole4score+
        incoming.hole5score+
        incoming.hole6score+
        incoming.hole7score+
        incoming.hole8score+
        incoming.hole9score
    )

    backninescore = (
        incoming.hole10score+
        incoming.hole11score+
        incoming.hole12score+
        incoming.hole13score+
        incoming.hole14score+
        incoming.hole15score+
        incoming.hole16score+
        incoming.hole17score+
        incoming.hole18score
    )

    score = (
        incoming.hole1score+
        incoming.hole2score+
        incoming.hole3score+
        incoming.hole4score+
        incoming.hole5score+
        incoming.hole6score+
        incoming.hole7score+
        incoming.hole8score+
        incoming.hole9score+
        incoming.hole10score+
        incoming.hole11score+
        incoming.hole12score+
        incoming.hole13score+
        incoming.hole14score+
        incoming.hole15score+
        incoming.hole16score+
        incoming.hole17score+
        incoming.hole18score
    )

    avg_fairway_front = 0
    avg_fairway_back = 0
    avg_gir_front = 0
    avg_gir_back = 0
    avg_fir_front = 0
    avg_fir_back = 0

    for key, value in incoming:
        fairway_front_total = 0
        fairway_back_total = 0
        gir_front_total = 0
        gir_back_total = 0
        fir_front_total = 0
        fir_back_total = 0
        if incoming.hole1fairway is True:
            fairway_front_total += 1
        if incoming.hole2fairway is True:
            fairway_front_total += 1
        if incoming.hole3fairway is True:
            fairway_front_total += 1
        if incoming.hole4fairway is True:
            fairway_front_total += 1
        if incoming.hole5fairway is True:
            fairway_front_total += 1
        if incoming.hole6fairway is True:
            fairway_front_total += 1
        if incoming.hole7fairway is True:
            fairway_front_total += 1
        if incoming.hole8fairway is True:
            fairway_front_total += 1
        if incoming.hole9fairway is True:
            fairway_front_total += 1
        if incoming.hole10fairway is True:
            fairway_back_total += 1
        if incoming.hole11fairway is True:
            fairway_back_total += 1
        if incoming.hole12fairway is True:
            fairway_back_total += 1
        if incoming.hole13fairway is True:
            fairway_back_total += 1
        if incoming.hole14fairway is True:
            fairway_back_total += 1
        if incoming.hole15fairway is True:
            fairway_back_total += 1
        if incoming.hole16fairway is True:
            fairway_back_total += 1
        if incoming.hole17fairway is True:
            fairway_back_total += 1
        if incoming.hole18fairway is True:
            fairway_back_total += 1
        if fairway_front_total == 0:
            avg_fairway_front = 0
        else:
            avg_fairway_front = float(fairway_front_total/9)
        if fairway_back_total == 0:
            avg_fairway_back = 0
        else:
            avg_fairway_back = float(fairway_back_total/9)
        if incoming.hole1gir is True:
            gir_front_total += 1
        if incoming.hole2gir is True:
            gir_front_total += 1
        if incoming.hole3gir is True:
            gir_front_total += 1
        if incoming.hole4gir is True:
            gir_front_total += 1
        if incoming.hole5gir is True:
            gir_front_total += 1
        if incoming.hole6gir is True:
            gir_front_total += 1
        if incoming.hole7gir is True:
            gir_front_total += 1
        if incoming.hole8gir is True:
            gir_front_total += 1
        if incoming.hole9gir is True:
            gir_front_total += 1
        if incoming.hole10gir is True:
            gir_back_total += 1
        if incoming.hole11gir is True:
            gir_back_total += 1
        if incoming.hole12gir is True:
            gir_back_total += 1
        if incoming.hole13gir is True:
            gir_back_total += 1
        if incoming.hole14gir is True:
            gir_back_total += 1
        if incoming.hole15gir is True:
            gir_back_total += 1
        if incoming.hole16gir is True:
            gir_back_total += 1
        if incoming.hole17gir is True:
            gir_back_total += 1
        if incoming.hole18gir is True:
            gir_back_total += 1
        if gir_front_total == 0:
            avg_fir_front = 0
        else:
            avg_gir_front = float(gir_front_total/9)
        if gir_back_total == 0:
            avg_gir_back = 0
        else:
            avg_gir_back = float(gir_back_total/9)
        if incoming.hole1fir is True:
            fir_front_total += 1
        if incoming.hole2fir is True:
            fir_front_total += 1
        if incoming.hole3fir is True:
            fir_front_total += 1
        if incoming.hole4fir is True:
            fir_front_total += 1
        if incoming.hole5fir is True:
            fir_front_total += 1
        if incoming.hole6fir is True:
            fir_front_total += 1
        if incoming.hole7fir is True:
            fir_front_total += 1
        if incoming.hole8fir is True:
            fir_front_total += 1
        if incoming.hole9fir is True:
            fir_front_total += 1
        if incoming.hole10fir is True:
            fir_back_total += 1
        if incoming.hole11fir is True:
            fir_back_total += 1
        if incoming.hole12fir is True:
            fir_back_total += 1
        if incoming.hole13fir is True:
            fir_back_total += 1
        if incoming.hole14fir is True:
            fir_back_total += 1
        if incoming.hole15fir is True:
            fir_back_total += 1
        if incoming.hole16fir is True:
            fir_back_total += 1
        if incoming.hole17fir is True:
            fir_back_total += 1
        if incoming.hole18fir is True:
            fir_back_total += 1
        if fir_front_total == 0:
            avg_fir_front = 0
        else:
            avg_fir_front = float(fir_front_total/9)
        if fir_back_total == 0:
            avg_fir_back = 0
        else:
            avg_fir_back = float(fir_back_total/9)


    avg_putts_front_rounded = round(avg_puttsfrontnine, 2)
    avg_putts_back_rounded = round(avg_puttsbacknine, 2)
    avg_fairway_front_rounded = round(avg_fairway_front, 2)
    avg_fairway_back_rounded = round(avg_fairway_back, 2)
    avg_gir_front_rounded = round(avg_gir_front, 2)
    avg_gir_back_rounded = round(avg_gir_back, 2)
    avg_fir_front_rounded = round(avg_fir_front, 2)
    avg_fir_back_rounded = round(avg_fir_back, 2)
    player_id = account_data["id"]


    print("-----------------------------------------------")
    print("average putts front: ", avg_putts_front_rounded)
    print("average putts back: ", avg_putts_back_rounded)
    print("average fairways: ", avg_fairway_back_rounded)
    print("average GIRs front: ", avg_gir_front_rounded)
    print("average GIRs back: ", avg_gir_back_rounded)
    print("average FIRs front: ", avg_fir_front_rounded)
    print("average FIRs back: ", avg_fir_back_rounded)
    print("front nine score: ", frontninescore)
    print("back nine score: ", backninescore)
    print("score: ", score)
    print('-----------------------------------------------')


    return repo.create_eighteenscore(
        incoming,
        player_id,
        avg_fairway_front_rounded,
        avg_fairway_back_rounded,
        avg_putts_front_rounded,
        avg_putts_back_rounded,
        avg_gir_front_rounded,
        avg_gir_back_rounded,
        avg_fir_front_rounded,
        avg_fir_back_rounded,
        frontninescore,
        backninescore,
        score
        )
