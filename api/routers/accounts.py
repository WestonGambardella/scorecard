import boto3
from botocore.exceptions import ClientError

from fastapi import (
    Depends,
    APIRouter,
    Request,
    UploadFile,
)
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator

from pydantic import BaseModel

from queries.accounts import (
    Account,
    AccountOutWithHashedPassword,
    AccountRepo,
    AccountOut,
    Error,
)
import os
from typing import Union


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOutWithHashedPassword


class AccountTokenPartDeux(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.get("/api/accounts", response_model=list[AccountOut, Error])
async def get_all_accounts(
    repo: AccountRepo = Depends(),
):
    return repo.get_all()


@router.get("/token", response_model=AccountTokenPartDeux | None)
async def get_token(
    request: Request,
    account: Account = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }


@router.post("/api/accounts", response_model=Union[Account, Error])
async def create_account(
    info: Account,
    repo: AccountRepo = Depends(),
) -> Union[Account, Error]:
    hashed_password = authenticator.hash_password(info.password)
    return repo.create_account(info, hashed_password)


@router.put("/api/accounts", response_model=Union[AccountOut, Error])
def update_account(
    info: Account,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: AccountRepo = Depends(),
) -> Union[AccountOut, Error]:
    account_id = account_data["id"]
    return repo.update_account(account_id, info)


@router.delete("/api/accounts", response_model=bool)
def delete_account(
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: AccountRepo = Depends(),
) -> bool:
    account_id = account_data["id"]
    return repo.delete_account(account_id)


@router.get("/api/user-account", response_model=AccountOut)
def get_one_account(
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: AccountRepo = Depends(),
) -> Union[AccountOut, Error]:
    account_id = account_data["id"]
    return repo.get_one_account(account_id)

"____________________________________THIS IS FOR THE PROFILE PICS_______________________________________"
s3_bucket_name = "scorecardbucket123"
aws_region = "us-east-2"
aws_access_key_id = "AKIAUKRWKHB46IM7LEYV"
aws_secret_access_key = "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"


@router.post("/api/upload-image")
def upload_image_to_s3(file: UploadFile) -> str:
    s3_object_key = file.filename
    s3_url = f"https://{s3_bucket_name}.s3.{aws_region}.amazonaws.com/{s3_object_key}"

    # Initialize an S3 client
    s3_client = boto3.client(
        "s3",
        aws_access_key_id=aws_access_key_id,
        aws_secret_access_key=aws_secret_access_key,
        region_name=aws_region
    )

    try:
        # Upload the file to S3
        s3_client.upload_fileobj(
            file.file,          # Uploaded file's file-like object
            s3_bucket_name,     # S3 bucket name
            s3_object_key       # Object key (filename) in the S3 bucket
        )

        return s3_url  # Return the S3 URL of the uploaded image

    except ClientError as e:
        # Handle errors (e.g., connection error, permissions issues)
        print(f"Error uploading image to S3: {e}")
        raise e