from fastapi import Depends, APIRouter, HTTPException
from authenticator import authenticator
import os
import requests
from queries.ninescorecard import nineScoreIn, nineScoreOut, nineScoreRepo
from datetime import datetime

router = APIRouter()




@router.get("/api/nine-hole-scores", response_model=list[nineScoreOut])
async def get_all_ninescorecards(
    repo: nineScoreRepo = Depends(),
):
    return repo.get_all_nine_scorecards()



@router.get("/api/user/nine-hole-scores", response_model=list[nineScoreOut])
async def get_all_ninescorecards_from_user(
    repo: nineScoreRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data)
):
    player_id = account_data["id"]
    return repo.get_all_nine_scores_user(player_id)



@router.post("/api/create_ninescorecard", response_model=nineScoreOut)
async def create_ninescore(
    data: nineScoreIn,
    account_data: dict = Depends(authenticator.get_current_account_data),
    repo: nineScoreRepo = Depends()
) -> nineScoreOut:
    player_id = account_data["id"]
    round_date = datetime.today().date()
    return repo.create_ninescore(data, player_id, round_date)
