CREATE TABLE IF NOT EXISTS accounts (
    id SERIAL PRIMARY KEY,
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    email VARCHAR(100) UNIQUE NOT NULL,
    username VARCHAR(15) UNIQUE NOT NULL,
    home_course VARCHAR(100),
    hashed_password VARCHAR(200) NOT NULL,
    created VARCHAR(50) NOT NULL,
    is_active BOOLEAN NOT NULL
);


CREATE TABLE IF NOT EXISTS ninescorecard (
    id SERIAL PRIMARY KEY,
    course_name VARCHAR(100) NOT NULL,
    course_city VARCHAR(100) NOT NULL,
    course_state VARCHAR(100) NOT NULL,
    tees VARCHAR(30) NOT NULL,
    par_differential INT NOT NULL,
    avg_putts DEC,
    avg_gir DEC,
    avg_fir DEC,
    score INT,
    player_id INT REFERENCES accounts(id),
    round_date DATE NOT NULL
);


CREATE TABLE IF NOT EXISTS friends (
    id SERIAL PRIMARY KEY,
    sender INT REFERENCES accounts(id),
    receiver INT REFERENCES accounts(id),
    request_status VARCHAR(30)
);


CREATE TABLE IF NOT EXISTS eighteenscorecard (
    id SERIAL PRIMARY KEY,
    player_id INT REFERENCES accounts(id),
    course_name VARCHAR(100) NOT NULL,
    course_city VARCHAR(100) NOT NULL,
    course_state VARCHAR(100) NOT NULL,
    round_date DATE NOT NULL,
    tees VARCHAR(30) NOT NULL,
    avg_puttsfrontnine DEC,
    avg_girfrontnine DEC,
    avg_firfrontnine DEC,
    avg_puttsbacknine DEC,
    avg_girbacknine DEC,
    avg_firbacknine DEC,
    frontninescore INT,
    backninescore INT,
    score INT
)
